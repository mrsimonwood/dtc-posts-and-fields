<?php

abstract class WDTC_Shortcode_Handler {
	/*
	 * Sets up the shortcode parameter and calls the method to produce the output
	 *
	 */
	public function handle_shortcode($atts) {
		$atts = shortcode_atts($this->get_pairs(), $atts);
		return $this->shortcode_output($atts);
	}
	
	/*
	 * Sets the parameters and the defaults for the shortcode
	 *
	 */
	abstract protected function get_pairs();
	
	/*
	 * Return the output for the shortcode.
	 *
	 */
	abstract protected function shortcode_output($atts);
}


/*
 * Class for displaying taxonomy tag clouds
 *
 * @since DTC Posts and Fields 0.5.1
 */
class WDTC_Taxonomy_Tag_Cloud extends WDTC_Shortcode_Handler {

	/*
	 * Sets the parameters and the defaults for the shortcode
	 *
	 */
	protected function get_pairs() {
		return array( 'taxonomy' => '', 'number' => '0', 'min_count' => '0');
	}

	/*
	 * Output the HTML for the tag cloud
	 *
	 * @param array $atts the attributes provided along with the shortcode
	 *
	 * @return string html to display the tag cloud
	 */
	protected function shortcode_output($atts) {
		$terms = get_terms($atts['taxonomy']);
		if (empty($terms) || !is_array($terms))
			return;
		$min_count = $atts['min_count'];
		$included_terms = array();
		foreach ($terms as $term) {
			$term->link = get_term_link($term,$atts['taxonomy']);
			if ($term->count>=$min_count)
				$included_terms[]=$term;
		}
		$args = array('number' => $atts['number']);
		$html = wp_generate_tag_cloud( $included_terms, $args );
		return $html;
	}
}

//		$taxonomy_tag_cloud = new WDTC_Taxonomy_Tag_Cloud;
//		add_shortcode('tag_cloud', array($taxonomy_tag_cloud, 'handle_shortcode'));