<?php


class WDTC_Context_Checker {

	private $tax_info;
	
	private $post_type_with_tax_info;
	
	private $post_type_info;
	
	private function get_tax_info() {
		return $this->tax_info;
	}
	
	public function set_tax_info(array $tax) {
		$this->tax_info = $tax;
	}
	
	private function get_post_type_with_tax_info() {
		return $this->post_type_with_tax_info;
	}
	
	public function set_post_type_with_tax_info(array $tax) {
		$this->post_type_with_tax_info = $tax;
	}
	
	private function get_post_type_info() {
		return $this->post_type_info;
	}
	
	public function set_post_type_info($show) {
		$this->post_type_info = $show;
	}
	
	public function in_context() {
		global $post;
		$in_context = false;
		if (is_tax()) {
			if (in_array(get_queried_object()->taxonomy, $this->get_tax_info())) {
				if (!is_post_type_archive())
					$in_context = true;
			}
		}
		if (is_post_type_archive()) {
			if (is_tax()) {
				if (in_array(get_queried_object()->term_id, $this->get_post_type_with_tax_info()))
					$in_context = true;
			} else {
				if ($this->get_post_type_info())
					$in_context = true;
			}
		}
		return $in_context;
	}
}
/*
 * Call it something like this
 *
 * $hider = new WDTC_Display_With_Content_Hidden;
 * $hider->add_context('profile', array('institution', 'pathway', 'topic'), array('institution'), true);
 * $hider->activate_hooks(true);
 */

abstract class WDTC_Display_Modifier {
	/*
	 * Activate or deactivate hooks for the class
	 *
	 * @param bool $activate whether to activate rather than deactivate the hooks
	 */
	public function activate_hooks($activate) {
		if ($activate) {
			$action = 'add_';
		} else {
			$action = 'remove_';
		}
		foreach($this->get_hooks_info() as $hook_args) {
			call_user_func_array($action . array_shift($hook_args),$hook_args);
		}
	}
	
	abstract protected function in_context();

	abstract protected function get_hooks_info();
}

abstract class WDTC_Display_Post_Type_Modifier extends WDTC_Display_Modifier {

	private $contexts;

	public function add_context($post_type, array $tax_info, array $post_type_with_tax_info, $post_type_info) {
		$context_checker = new WDTC_Context_Checker;
		$context_checker->set_tax_info($tax_info);
		$context_checker->set_post_type_with_tax_info($post_type_with_tax_info);
		$context_checker->set_post_type_info($post_type_info);
		$this->contexts[$post_type] = $context_checker;
	}
	
	protected function get_context($post_type) {
		if (isset($this->contexts[$post_type]))
			return $this->contexts[$post_type];
	}
	
	protected function get_contexts() {
		return $this->contexts;
	}
	
	protected function in_context() {
		if ($this->get_context(get_post_type()))
			if ($this->get_context(get_post_type())->in_context())
				return true;
		return false;
	}
}


class WDTC_Display_With_Content_Hidden extends WDTC_Display_Post_Type_Modifier {
	/*
	 * The hooks for the class to activate or deactivate
	 */
	protected function get_hooks_info() {
		$hooks_args[] = array('filter', 'the_content', array(&$this,'blank'));
		return $hooks_args;
	}
	
	/*
	 * Returns an empty string if in context
	 *
	 * @param string $whatever anything
	 *
	 * @return string empty string
	 */
	public function blank($whatever) {
		if ($this->in_context())
			return '';
		return $whatever;
	}
}


class WDTC_Display_With_Custom_Thumb_Size extends WDTC_Display_Post_Type_Modifier {
	/*
	 * The hooks for the class to activate or deactivate
	 */
	protected function get_hooks_info() {
		$hooks_args[] = array('filter', 'post_thumbnail_html', array(&$this,'post_thumbnail_html'), 1, 5);
		return $hooks_args;
	}
	
	/*
	 * Generate the thumbnail for the post - or, if it doesn't have one, fetch a
	 * placeholder image
	 *
	 * @param string $html the default html for the thumbnail
	 * @param integer $post_id the id for the post to which the thumbnail is attached
	 * @param integer $post_thumbnail_id the id for the thumbnail itself
	 * @param string|array $size image size
	 * @param string|array $attr attributes for the image markup
	 *
	 * @return string html img element
	 */
	public function post_thumbnail_html($html, $post_id, $post_thumbnail_id, $size, $attr ) {
		if ($this->in_context()) {
			$has_thumb = false;
			if ( '' != $html)
				$has_thumb = true; 
			if ($has_thumb) {
				$thumb_size =$this->custom_thumb_size();
				if ($thumb_size == '')
					$thumb_size = 'thumbnail';
				$html = wp_get_attachment_image( $post_thumbnail_id, $thumb_size, false, $attr );
    			$html = $this->make_permalink($html);
			} else {
	    		$html = $this->custom_thumb_default();
				$html = $this->make_permalink($html);
			}
		}
		return $html;
	}

	/*
	 * Custom size for thumbnail image
	 *
	 * @return string image size
	 */
	private function custom_thumb_size() {
		return apply_filters('wdtc_custom_thumb_size', '');
	}
	
	/*
	 * Default image to use for the thumbnail if the post doesn't have one
	 *
	 * @return string html tag with image
	 */
	private function custom_thumb_default() {
		return apply_filters('wdtc_custom_thumb_default', '');
	}
	
	/*
	 * Given some string make it into a hyperlink to the post
	 *
	 * @param string $text the text for the hyperlink
	 *
	 * @return string the html hyperlink
	 */
	private function make_permalink ($text) {
		return '<a href="' . get_permalink() . '">' . $text . '</a>';
	}
}


class WDTC_Display_With_Formatted_Name_In_Title extends WDTC_Display_Post_Type_Modifier {
	/*
	 * The hooks for the class to activate or deactivate
	 */
	protected function get_hooks_info() {
		$hooks_args[] = array('filter', 'the_title', array(&$this,'format_name_in_title') );
		return $hooks_args;
	}
	
	/*
	 * Modify the title contents by reformatting names in the surname, firstname format
	 */
	public function format_name_in_title($title) {
		if ($this->in_context())
			$title = WDTC_String_Formatter::formatname($title);
		return $title;
	}
}


class WDTC_Display_With_Additional_Classes extends WDTC_Display_Post_Type_Modifier {
	
	private $classes;
	
	/*
	 * The hooks for the class to activate or deactivate
	 */
	protected function get_hooks_info() {
	 	$hooks_args[] = array('filter', 'post_class', array(&$this,'post_class'));
		return $hooks_args;
	}
	
	public function set_classes(array $classes) {
		$this->classes = $classes;
	}
	
	private function get_classes() {
		return $this->classes;
	}
	
	/*
	 * Add custom classes
	 *
	 * @param array $classes existing classes
	 *
	 * @return array classes including new custom classes
	 */
	public function post_class($classes) {
		if ($this->in_context())
			$classes = array_merge($classes, $this->get_classes());
		return $classes;
	}
}
class WDTC_Display_With_Fields_Hidden extends WDTC_Display_Post_Type_Modifier {
	
	private $fields;
	
	/*
	 * The hooks for the class to activate or deactivate
	 */
	protected function get_hooks_info() {
		foreach ($this->get_contexts() as $post_type => $context) {
			foreach ($this->get_fields() as $field)
				$hooks_args[] = array('filter', 'wdtc_' . $post_type . '_' . $field . '_field_html',array(&$this,'blank'));
		}
		return $hooks_args;
	}
	
	public function set_fields(array $fields) {
		$this->fields = $fields;
	}
	
	private function get_fields() {
		return $this->fields;
	}
	
	/*
	 * Returns an empty string if in context
	 *
	 * @param string $whatever anything
	 *
	 * @return string empty string
	 */
	public function blank($whatever) {
		if ($this->in_context())
			return '';
		return $whatever;
	}
}

abstract class WDTC_Display_Archive_Modifier extends WDTC_Display_Modifier {
	private $contexts;
	
	public function set_contexts(array $contexts) {
		$this->contexts = $contexts;
	}
	
	protected function get_contexts() {
		return $this->contexts;
	}
	
	protected function in_context() {
		foreach ($this->get_contexts() as $context) {
			if (is_tax($context) || is_post_type_archive($context))
				return true;
		}
		return false;
	}
}

abstract class WDTC_Display_For_Some_Posts_in_Archive extends WDTC_Display_Archive_Modifier {
	/*
	 * Array containing details of previous post to that currently displayed
	 *
	 * @var array
	 */
	private $previous_post;

	/*
	 * Getter for info on the previous post
	 *
	 * @param string the name of the info required
	 *
	 * @return array info about the previous post
	 */
	protected function get_previous_post_info($info) {
		return $this->previous_post[$info];
	}	
	
	/*
	 * Setter for info on the previous post
	 *
	 * @param string the name of the info to set 
	 * @param mixed the value of the info to set
	 */
	protected function set_previous_post_info($info, $value) {
		$this->previous_post[$info] = $value;
	}

}

class WDTC_Display_With_Subhead extends WDTC_Display_For_Some_Posts_in_Archive {
	private $subhead_taxonomy;
	
	public function set_subhead_taxonomy($tax) {
		$this->subhead_taxonomy = $tax;
	}
	
	private function get_subhead_taxonomy() {
		return $this->subhead_taxonomy;
	}

	/*
	 * The hooks for the class to activate or deactivate
	 *
	 * @param bool $activate whether to activate rather than deactivate the hooks
	 */
	protected function get_hooks_info() {	
		$hooks_args[] = array('filter', 'wdtc_archive_subhead', array(&$this,'subhead'));
		return $hooks_args;
	}

	/*
	 * Display a subheading if and only if it is changed from the previous post
	 */
	public function subhead($subhead) {
		if ($this->in_context()) {
			global $post;
			$new_subhead = $this->subhead_content($post);
			if ($new_subhead != $this->get_previous_post_info('subhead')) {
				$subhead .= $new_subhead;
				$this->set_previous_post_info('subhead', $new_subhead);
			}
		}
		return $subhead;
	}
	
	/*
     * Generate the wording for the subheading to use for the current post
     *
     * @param $post WP_Post 
     *
     * @return string wording for the subheading
	 */
	protected function subhead_content($post) {
		if (is_post_type_archive())
			return get_the_term_list( $post->ID, $this->get_subhead_taxonomy(), '',', ','' );
		if (is_tax())
			return get_post_type_object(get_post_type($post))->labels->name;
	}
}

class WDTC_Display_With_Modified_Archive_Page_Title extends WDTC_Display_Archive_Modifier {
	/*
	 * For archive pages, this function defines the titles to use.
	 * Uses the wdtc_archive_page_title hook as implemented in the esrcwalesdtc theme.
	 *
	 * @param string $page_title the page title
	 */
	function archive_page_title($page_title) {
		if ($this->in_context()) {
			if (is_tax()) {
				$tax_name = get_query_var( 'taxonomy' );
				if ($this->prepend_terms_with_tax_singular_name()) {
					$taxonomy = get_taxonomy($tax_name);
					$pre = $taxonomy->labels->singular_name . ': ';
				}
				$term = get_term_by( 'slug', get_query_var( 'term' ), $tax_name );
				$new_page_title = $pre . $term->name;
			}
			if (is_post_type_archive()) {
				$post_type_name = get_post_type_object(get_post_type())->labels->name;
				if ($new_page_title) {
					$new_page_title .= " " . $post_type_name;
				} else {
					$new_page_title = $post_type_name;
				}
			}
			$page_title = $new_page_title;
		}
		return $page_title;
	}

	
	/*
	 * The hooks for the class to activate or deactivate
	 *
	 * @param bool $activate whether to activate rather than deactivate the hooks
	 */
	protected function get_hooks_info() {	
		$hooks_args[] = array('filter', 'wdtc_archive_page_title', array(&$this, 'archive_page_title'));
		return $hooks_args;
	}
	
	/*
	 * Whether to put the taxonomy name in the archive title before the taxonomy term.
	 * Override to enable
	 *
	 * @return bool false
	 */
	protected function prepend_terms_with_tax_singular_name() {
		return false;
	}
}

class WDTC_Display_With_Modified_Archive_Page_Title_With_Tax_Name extends WDTC_Display_With_Modified_Archive_Page_Title {
	/*
	 * Whether to put the taxonomy name in the archive title before the taxonomy term.
	 *
	 * @return bool true
	 */
	protected function prepend_terms_with_tax_singular_name() {
		return true;
	}
}

class WDTC_Display_Taxonomy_Archive_With_Term_Description extends WDTC_Display_For_Some_Posts_in_Archive {

	private $post_type_to_display_term_description_after;

	public function set_post_type_to_display_term_description_after($post_type) {
		$this->post_type_to_display_term_description_after = $post_type;
	}

	private function get_post_type_to_display_term_description_after() {
		return $this->post_type_to_display_term_description_after;
	}

	/*
	 * Check whether appropriate to display term description
     */	
	public function check_and_display_term_description() {
		if ($this->in_context()) {
			global $post;
			$post_type = get_post_type($post);
			if (($this->get_previous_post_info('post_type') == $this->get_post_type_to_display_term_description_after() && $post_type != $this->get_post_type_to_display_term_description_after()) || !$this->get_post_type_to_display_term_description_after()) {
				$this->display_term_description(false);
			}
			$this->set_previous_post_info('post_type', $post_type);
		}
	}
	
	/*
	 * Display description for the term
     */	
	public function display_term_description($after_loop = true) {
		if ($this->in_context() && !$this->get_previous_post_info('term_description_displayed')) {
				echo '<div class="term-description term-description-before-loop">';
				echo term_description();
				echo '</div>';
				$this->set_previous_post_info('term_description_displayed', true);
		}
	}

	/*
	 * The hooks for the class to activate or deactivate
	 *
	 * @param bool $activate whether to activate rather than deactivate the hooks
	 */
	protected function get_hooks_info() {
		$this->set_previous_post_info('term_description_displayed', false);
		$hooks_args[] = array('action', 'wdtc_archive_before_post', array(&$this,'check_and_display_term_description'));
		$hooks_args[] = array('action', 'loop_end', array(&$this,'display_term_description'));
		return $hooks_args;
	}
}

class WDTC_Display_By_Post_Type_In_Archive extends WDTC_Display_Archive_Modifier {
	
	/*
	 * On taxonomy archive pages with multiple post types, order by post type first (then
	 * by title)
	 * 
	 * @param string $orderby MySQL query
	 * 
	 * @return string MySQL query
	 */ 
	function order_posts_by_post_type ( $orderby ){
		global $wpdb;
		if($this->in_context()) 
			$orderby =  $wpdb->prefix."posts.post_type DESC, {$wpdb->prefix}posts.post_title ASC";
		return  $orderby;
	}
	
	/*
	 * The hooks for the class to activate or deactivate
	 *
	 * @param bool $activate whether to activate rather than deactivate the hooks
	 */
	protected function get_hooks_info() {
		$hooks_args[] = array('filter', 'posts_orderby', array(&$this, 'order_posts_by_post_type'));
		return $hooks_args;
	}
}