<?php

/*
 * Parent class customising the display the posts within archive pages...
 *
 * @since Doctoral_Training_Post_Types 0.4
 */

abstract class WDTC_Context_Customiser extends WDTC_Standard {

	/*
	 * Info on how to customise the display of archives/posts
	 *
	 * @var array
	 */
	private $args;
	
	/*
	 * Constructor
	 *
	 * @param string $id the identifier for the thing to be customised
	 * @param array $args the options for customisation
	 */
	public function __construct($id, array $args) {
		parent::__construct($id,'');
		$this->set_args($args, $this->get_default_args());
	}
	
	/*
	 * Getter for the customisation info
	 *
	 * @param string the info to get
	 *
	 * @return string the info
	 */
	protected function get_arg($arg) {
		return $this->args[$arg];
	}

	
	/*
	 * Activate hooks for the class
	 */
	public function init() {
		$this->activate_hooks(true);
	}
	
	/*
	 * Deactivate hooks for the class
	 */
	public function remove() {
		$this->activate_hooks(false);
	}
	
	/*
	 * Activate or deactivate hooks for the class
	 *
	 * @param bool $activate whether to activate rather than deactivate the hooks
	 */
	private function activate_hooks($activate) {
		if ($activate) {
			$action = 'add_';
		} else {
			$action = 'remove_';
		}
		foreach($this->get_hooks_info() as $hook_args) {
			call_user_func_array($action . array_shift($hook_args),$hook_args);
		}
	}
	
	/*
	 * Abstract method to override to get the hooks info for the class
	 *
	 * @param bool $activate whether to include arguments used for activating
	 */
	abstract protected function get_hooks_info();
	
	/*
	 * Abstract funciton to override with a check on whether the archive/post should
	 * be customised in the present context
	 */	
	abstract protected function in_context();

	/*
	 * The default options for customisations
	 *
	 * @return array the defaults
	 */
	abstract protected function get_default_args();

	/*
	 * Set the defaults for the customisation info
	 *
	 * @param array $args any values to overwrite the defaults
	 * @param array $defaults the default values for the customisation info
	 *
	 * @return array array of defaults overwritten where new values were supplied
	 */
	protected function args_defaults($args, $defaults) {
        $args = (array)$args;
		$out = array();
		foreach ($defaults as $key => $default) {
			if ( array_key_exists($key, $args) )
				$out[$key] = $args[$key];
			else
				$out[$key] = $default;
		}
		return $out;
	}

	/*
	 * Set the customisation info with values supplied or defaults
	 *
	 * @param array $args any values to overwrite the defaults
	 * @param array $defaults the default values for the customisation info
	 */
	protected function set_args($args, $defaults) {
        $this->args = $this->args_defaults($args, $defaults);
	}
}

/*
 * Customise how the posts of a particular post type are displayed...
 *
 * @since Doctoral_Training_Post_Types 0.4
 */
class WDTC_Post_Type_Customiser extends WDTC_Context_Customiser {

	/*
	 * The default options for customisations
	 *
	 * @return array the defaults
	 */
	protected function get_default_args() {
		return array('format_name_in_title'=>false, 'hide_content'=>false, 'custom_thumb_size'=>false, 'placeholder_thumnail'=>false, 'classes'=>array(), 'hide_fields'=>array());
	}
	
	/*
	 * The hooks for the class to activate or deactivate
	 */
	protected function get_hooks_info() {
		$hooks_args = array();
		//if ($this->get_arg('format_name_in_title'))
		//	$hooks_args[] = array('filter', 'the_title', array(&$this,'format_name_in_title') );
		//if ($this->get_arg('hide_content'))
		//	$hooks_args[] = array('filter', 'the_content', array(&$this,'blank'));
		//if ($this->get_arg('custom_thumb_size') || $this->get_arg('placeholder_thumnail'))
		//	$hooks_args[] = array('filter', 'post_thumbnail_html', array(&$this,'post_thumbnail_html'), 1, 5);
		//if ($this->get_arg('classes') != NULL)
		//	$hooks_args[] = array('filter', 'post_class', array(&$this,'post_class'));
		if ($this->get_arg('hide_fields') != NULL && is_array($this->get_arg('hide_fields')) ) {
			foreach ($this->get_arg('hide_fields') as $field) {
				//$hooks_args[] = array('filter', 'wdtc_' . $this->get_id() . '_' . $field . '_field_html',array(&$this,'blank'));
			}
		}
		return $hooks_args;
	}

	/*
	 * Check whether the archive is of the specified taxonomy.
	 *
	 * @return bool true if the condition is met
	 */
	protected function in_context() {
		global $post;
		if (get_post_type($post) == $this->get_id())
			return true;
		return false;
	}

	/*
	 * Generate the thumbnail for the post - or, if it doesn't have one, fetch a
	 * placeholder image
	 *
	 * @param string $html the default html for the thumbnail
	 * @param integer $post_id the id for the post to which the thumbnail is attached
	 * @param integer $post_thumbnail_id the id for the thumbnail itself
	 * @param string|array $size image size
	 * @param string|array $attr attributes for the image markup
	 *
	 * @return string html img element
	 */
	public function post_thumbnail_html($html, $post_id, $post_thumbnail_id, $size, $attr ) {
		if ($this->in_context()) {
			$has_thumb = false;
			if ( '' != $html)
				$has_thumb = true; 
			if ($this->get_arg('custom_thumb_size') && $has_thumb) {
				$thumb_size =$this->custom_thumb_size();
				if ($thumb_size == '')
					$thumb_size = 'thumbnail';
				$html = wp_get_attachment_image( $post_thumbnail_id, $thumb_size, false, $attr );
    			$html = $this->make_permalink($html);
			}
			if ($this->get_arg('placeholder_thumnail') && !$has_thumb) {
	    		$html = $this->custom_thumb_default();
				$html = $this->make_permalink($html);
			}
		}
		return $html;
	}

	/*
	 * Custom size for thumbnail image
	 *
	 * @return string image size
	 */
	private function custom_thumb_size() {
		return apply_filters('wdtc_custom_thumb_size', '');
	}
	
	/*
	 * Default image to use for the thumbnail if the post doesn't have one
	 *
	 * @return string html tag with image
	 */
	private function custom_thumb_default() {
		return apply_filters('wdtc_custom_thumb_default', '');
	}
	
	/*
	 * Given some string make it into a hyperlink to the post
	 *
	 * @param string $text the text for the hyperlink
	 *
	 * @return string the html hyperlink
	 */
	private function make_permalink ($text) {
		return '<a href="' . get_permalink() . '">' . $text . '</a>';
	}

	/*
	 * Add custom classes
	 *
	 * @param array $classes existing classes
	 *
	 * @return array classes including new custom classes
	 */
	public function post_class($classes) {
		if ($this->in_context())
			$classes = array_merge($classes, $this->get_arg('classes'));
		return $classes;
	}
	
	/*
	 * Returns an empty string if in context
	 *
	 * @param string $whatever anything
	 *
	 * @return string empty string
	 */
	public function blank($whatever) {
		if ($this->in_context())
			return '';
		return $whatever;
	}

	/*
	 * Modify the title contents by reformatting names in the surname, firstname format
	 */
	public function format_name_in_title($title) {
		if ($this->in_context())
			$title = WDTC_String_Formatter::formatname($title);
		return $title;
	}
}

/*
 * Customise how a post of a particular post type is displayed when it appears in the 
 * archive of a particular taxonomy...
 *
 * @since Doctoral_Training_Post_Types 0.4
 */
 
class WDTC_Post_Type_In_Taxonomy_Archive_Customiser extends WDTC_Post_Type_Customiser {

	/*
	 * The taxonomy within whose archive page posts will be customised
	 *
	 * @var string
	 */
	private $context;
	
	/*
	 * Constructor
	 *
	 * @param string $id the post type for which posts will be customised
	 * @param string $context the taxonomy within whose archive page posts will be customised
	 * @param array $args info on how the posts will be customised
	 */
	public function __construct($id,$context,$args) {
		parent::__construct($id,$args);
		$this->context = $context;
	}

	/*
	 * The default options for customisations
	 *
	 * @return array the defaults
	 */
	protected function get_default_args() {
		return array_merge(array('apply_in_post_type_archive'=>true), parent::get_default_args());
	}
	
	/*
	 * Getter for the context
	 *
	 * @return string the context
	 */
	private function get_context() {
		return $this->context;
	}

	/*
	 * Check whether the post is of the specified post type and displayed within
	 * a taxonomy archive of the specified taxonomy.
	 *
	 * @return bool true if the conditions are met
	 */
	protected function in_context() {
		global $post;
		if (get_post_type($post) == $this->get_id() && is_tax($this->get_context()) && ( !is_post_type_archive() || $this->get_arg('apply_in_post_type_archive')))
			return true;
		return false;
	}
}


/*
 * Customise how a post of a particular post type is displayed when it appears in the 
 * archive of a particular taxonomy...
 *
 * @since Doctoral_Training_Post_Types 0.4
 */
 
class WDTC_Post_Type_In_Post_Type_Archive_Customiser extends WDTC_Post_Type_Customiser {
	
	/*
	 * The default options for customisations
	 *
	 * @return array the defaults
	 */
	protected function get_default_args() {
		return array_merge(array('apply_in_taxonomy_archive'=>true), parent::get_default_args());
	}

	/*
	 * Check whether the post is of the specified post type and displayed within
	 * a taxonomy archive of the specified taxonomy.
	 *
	 * @return bool true if the conditions are met
	 */
	protected function in_context() {
		global $post;
		if (get_post_type($post) == $this->get_id() &&  is_post_type_archive($this->get_id()) && ( !is_tax() || $this->get_arg('apply_in_taxonomy_archive')))
			return true;
		return false;
	}
}

abstract class WDTC_Archive_Customiser extends WDTC_Context_Customiser {

	/*
	 * Array containing details of previous post to that currently displayed
	 *
	 * @var array
	 */
	private $previous_post;
	
	/*
	 * The default options for customisations
	 *
	 * @return array the defaults
	 */
	protected function get_default_args() {	
		return array('subhead'=>false, 'modify_archive_page_title'=>false);
	}
	
	/*
	 * The hooks for the class to activate or deactivate
	 *
	 * @param bool $activate whether to activate rather than deactivate the hooks
	 */
	protected function get_hooks_info() {	
		if ($this->get_arg('subhead'))
			$hooks_args[] = array('filter', 'wdtc_archive_subhead', array(&$this,'subhead'));
		if ($this->get_arg('modify_archive_page_title'))
			$hooks_args[] = array('filter', 'wdtc_archive_page_title', array(&$this, 'archive_page_title'));
		return $hooks_args;
	}
	
	/*
	 * Getter for info on the previous post
	 *
	 * @param string the name of the info required
	 *
	 * @return array info about the previous post
	 */
	protected function get_previous_post_info($info) {
		return $this->previous_post[$info];
	}	
	
	/*
	 * Setter for info on the previous post
	 *
	 * @param string the name of the info to set 
	 * @param mixed the value of the info to set
	 */
	protected function set_previous_post_info($info, $value) {
		$this->previous_post[$info] = $value;
	}

	/*
	 * Display a subheading if and only if it is changed from the previous post
	 */
	public function subhead($subhead) {
		if ($this->in_context()) {
			global $post;
			if (method_exists($this, 'subhead_content')) {
				$new_subhead = $this->subhead_content($post);
				if ($new_subhead != $this->get_previous_post_info('subhead')) {
					$subhead .= $new_subhead;
					$this->set_previous_post_info('subhead', $new_subhead);
				}
			}
		}
		return $subhead;
	}

	/*
	 * For archive pages, this function defines the titles to use.
	 * Uses the wdtc_archive_page_title hook as implemented in the esrcwalesdtc theme.
	 *
	 * @param string $page_title the page title
	 */
	abstract public function archive_page_title($page_title);
}

/*
 * Customise how the archive of a particular taxonomy is displayed...
 *
 * @since Doctoral_Training_Post_Types 0.4
 */
class WDTC_Taxonomy_Archive_Customiser extends WDTC_Archive_Customiser {
	
	/*
	 * The default options for customisations
	 *
	 * @return array the defaults
	 */
	protected function get_default_args() {
		return array_merge(array('term_description_displayed'=>false, 'term_description_after_post_type'=>NULL, 'apply_in_post_type_archive'=>false, 'order_by_post_type'=>false), parent::get_default_args());
	}
	
	/*
	 * The hooks for the class to activate or deactivate
	 *
	 * @param bool $activate whether to activate rather than deactivate the hooks
	 */
	protected function get_hooks_info() {
		$hooks_args = parent::get_hooks_info();
		if ($this->get_arg('term_description_displayed')) {
			$this->set_previous_post_info('term_description_displayed', false);
			$hooks_args[] = array('action', 'wdtc_archive_before_post', array(&$this,'check_and_display_term_description'));
			$hooks_args[] = array('action', 'loop_end', array(&$this,'display_term_description'));
		}
		if ($this->get_arg('order_by_post_type'))
			$hooks_args[] = array('filter', 'posts_orderby', array(&$this, 'order_posts_by_post_type'));
		return $hooks_args;
	}

	/*
	 * Check whether the archive is of the specified taxonomy.
	 *
	 * @return bool true if the condition is met
	 */
	protected function in_context() {
		if (is_tax($this->get_id()) && ( !is_post_type_archive() || $this->get_arg('apply_in_post_type_archive')))
			return true;
		return false;
	}

	/*
     * Generate the wording for the subheading to use for the current post
     *
     * @param $post WP_Post 
     *
     * @return string wording for the subheading
	 */
	protected function subhead_content($post) {
		return get_post_type_object(get_post_type($post))->labels->name;
	}

	/*
	 * Check whether appropriate to display term description
     */	
	public function check_and_display_term_description() {
		if ($this->in_context()) {
			global $post;
			$post_type = get_post_type($post);
			if (($this->get_previous_post_info('post_type') == $this->get_arg('term_description_after_post_type') && $post_type != $this->get_arg('term_description_after_post_type')) || !$this->get_arg('term_description_after_post_type')) {
				$this->display_term_description();
			}
			$this->set_previous_post_info('post_type', $post_type);
		}
	}
	
	/*
	 * Display description for the term
     */	
	public function display_term_description() {
		if ($this->in_context() && !$this->get_previous_post_info('term_description_displayed')) {
				echo '<div class="term-description term-description-before-loop">';
				echo term_description();
				echo '</div>';
				$this->set_previous_post_info('term_description_displayed', true);
		}
	}
	
	/*
	 * On taxonomy archive pages with multiple post types, order by post type first (then
	 * by title)
	 * 
	 * @param string $orderby MySQL query
	 * 
	 * @return string MySQL query
	 */ 
	function order_posts_by_post_type ( $orderby ){
		global $wpdb;
		if($this->in_context()) 
			$orderby =  $wpdb->prefix."posts.post_type DESC, {$wpdb->prefix}posts.post_title ASC";
		return  $orderby;
	}
	
	/*
	 * For archive pages, this function defines the titles to use.
	 * Uses the wdtc_archive_page_title hook as implemented in the esrcwalesdtc theme.
	 *
	 * @param string $page_title the page title
	 */
	function archive_page_title($page_title) {
		if ($this->in_context()) {
			$term = get_term_by( 'slug', get_query_var( 'term' ), $this->get_id() );
			$page_title = $term->name;
		}
		return $page_title;
	}
}

class WDTC_Post_Type_Archive_Customiser extends WDTC_Archive_Customiser {

	/*
	 * The default options for customisations
	 *
	 * @return array the defaults
	 */
	protected function get_default_args() {
		return array_merge(array('subhead_taxonomy'=>NULL, 'apply_in_taxonomy_archive'=>false), parent::get_default_args());
	} 

	/*
	 * Check whether the archive is of the specified post type.
	 *
	 * @return bool true if the condition is met
	 */
	protected function in_context() {
		if (is_post_type_archive($this->get_id()) && ( !is_tax() || $this->get_arg('apply_in_taxonomy_archive')))
			return true;
		return false;
	}
	
	/*
     * Generate the wording for the subheading to use for the current post
     *
     * @param $post WP_Post 
     *
     * @return string wording for the subheading
	 */
	protected function subhead_content($post) {
		return get_the_term_list( $post->ID, $this->get_arg('subhead_taxonomy'), '',', ','' );
	}

	/*
	 * For archive pages, this function defines the titles to use.
	 * Uses the wdtc_archive_page_title hook as implemented in the esrcwalesdtc theme.
	 *
	 * @param string $page_title the page title
	 */
	function archive_page_title($page_title) {
		if ($this->in_context()) {
			$post_type_name = get_post_type_object($this->get_id())->labels->name;
			if (is_tax()) {
				$page_title .= " " . $post_type_name;
			} else {
				$page_title = $post_type_name;
			}
		}
		return $page_title;
	}
}