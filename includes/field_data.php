<?php


/*
 * Parent class for handling the loading and saving of field data...
 *
 * @since Doctoral_Training_Post_Types 0.5
 */

abstract class WDTC_Field_Data {

	/*
	 * Load the field value from the data source.
	 *
	 * @param string $field_id the identifier for the field to load data for
	 * @param string $source_id the id of the object for which the metadata is to be loaded
	 * @param string $context the context in which the data will be loaded
	 *
	 * @return string the field value (or false if unable to load) 
	 */
	abstract public function load_data($value, $field_id, $source_id, $context);
	
	/*
	 * Save the field value to the data source.
	 *
	 * @param string $field_id the identifier for the field to save data for
	 * @param string $destination_id the id of the object for which the metadata is to be saved
	 * @param string $value the value to save
	 *
	 * @return bool true if data saved
	 */
	abstract public function save_data($field_id, $destination_id, $value);
	
	/*
	 * If the value can be selected from a range of choices, what are those choices?
	 *
	 * @param string $field_id the identifier for the field to get choices for
	 *
	 * return array choices to walk through, if any (may be an array of objects)
	 */
	public function get_choices($choices, $field_id) {
		return $choices;
	}

	/*
	 * Override to modify the walker used to populate multiple choice fields
	 *
	 * @return Walker walker
	 */
	public function get_walker($walker) {
		return $walker;
	}

	/*
	 * Get the id for the instance of whatever object the data for the field is being 
	 * drawn from
	 *
	 * @return int ID
	 */
	abstract public function get_data_source_id();
}

class WDTC_Post_Meta_Data extends WDTC_Field_Data {
	
	/*
	 * Load the field value from the post meta data.
	 *
	 * @param string $value value to overwrite or modify on load
	 * @param string $field_id the identifier for the field to load data for
	 * @param string $source_id the post id for which the metadata is to be loaded
	 * 
	 * @return string the field value 
	 */
	public function load_data($value, $field_id, $source_id, $context, $options = array()) {
	 	if (!$source_id)
	 		$source_id = $this->get_data_source_id();
 		return get_post_meta($source_id, $field_id, true);
	}
		
	/*
	 * Save the field value to the post meta data.
	 *
	 * @param string $field_id the identifier for the field to save data for
	 * @param string $destination_id the post id for which the metadata is to be saved
	 * @param string $value the value to save
	 */
	public function save_data($field_id, $destination_id, $value) {
		update_post_meta( $destination_id, $field_id, $value);
	}
	
	/*
	 * Get the id for the instance of whatever object the data for the field is being 
	 * drawn from - in this case the post object
	 *
	 * @return int post ID
	 */
	public function get_data_source_id() {
		global $post;
		return $post->ID;
	}
}

abstract class WDTC_Tax_Data extends WDTC_Field_Data {

	/*
	 * Load the field value from term(s) for the field's taxonomy.
	 *
	 * @param string $value value to overwrite or modify on load
	 * @param string $field_id the identifier for the field/taxonomy to load data for
	 * @param string $source_id the id of the object for which the metadata is to be loaded
	 * 
	 * @return string the term(s, comma separated)
	 */
	public function load_data($value, $field_id, $source_id, $context, $options = array()) {
	 	if (!$source_id)
	 		$source_id = $this->get_data_source_id();
		if ($context == 'display') {
			//$args = array ('show' => true);
			if ($options['tax_display'] == 'ancestor' )
				$args = array ('show' => false);
			return WDTC_Term_Lists::get_the_term_ancestors_list($source_id,$field_id,'','; ','', $args);
		} else {
			$terms = wp_get_post_terms($source_id, $field_id, array());
			if ( !$terms || is_wp_error($terms) )
				return $value;
			return $this->terms_string($terms);
		}
	}
			
	/*
	 * Save the field value to the post meta data.
	 *
	 * @param string $field_id the identifier for the field to save data for
	 * @param string $destination_id the post id for which the metadata is to be saved
	 * @param string $value the value to save
	 */
	public function save_data($field_id, $destination_id, $value) {
		$terms = preg_split( "/[;,]+/", $value );
 		wp_set_object_terms( $destination_id, $terms, $field_id);
	}

	/*
	 * Implement to convert an array of terms into a string.
	 *
	 * @param array $terms array of terms
	 *
	 * @return string the term(s, comma separated)
	 */
	abstract protected function terms_string(array $terms);
	
	/*
	 * Get the id for the instance of whatever object the data for the field is being 
	 * drawn from - in this case the post object
	 *
	 * @return int post ID
	 */
	public function get_data_source_id() {
		global $post;
		return $post->ID;
	}
}

class WDTC_Hierarchical_Tax_Data extends WDTC_Tax_Data {

	/*
	 * Take an array of terms and return the slug for the first one.
	 *
	 * @param array $terms array of terms
	 *
	 * @return string term slug
	 */
	protected function terms_string(array $terms) {
		return $terms[0]->slug;
	}

	/*
	 * Get the taxonomy terms to choose from
	 *
	 * @param string $field_id the identifier for the taxonomy to get term choices for
	 *
	 * return array choices to walk through - terms
	 */
	public function get_choices($choices, $field_id) {
		return get_terms(array('taxonomy'=>$field_id));
	}

	/*
	 * Override the walker to use custom walker
	 *
	 * @return Walker_CategoryDropdown walker
	 */
	public function get_walker($walker) {
		return new Walker_CategoryDropdown;
	}
}

class WDTC_Nonhierarchical_Tax_Data extends WDTC_Tax_Data {

	/*
	 * Convert an array of terms into a string of comma separated term names.
	 *
	 * @param array $terms array of terms
	 *
	 * @return string term names, comma separated
	 */
	protected function terms_string(array $terms) {
		foreach ( $terms as $term )
			$term_names[] = $term->name;
		$terms_string = join( ', ', $term_names );
		$terms_string = esc_attr( $terms_string );
		return $terms_string;
	}
}


/*
 * Use profile data for a field. Extends WDTC_Field_Data from Doctoral Training Plugin
 *
 * @since DTC Posts and Fields 0.5
 */
abstract class WDTC_User_Data extends WDTC_Field_Data {

	/*
	 * Returns an array of user attributes that are stored in Wordpress' users table
	 * rather than user_meta
	 *
	 * @return array
	 */
	private function users_table_attributes() {
		return array(
			'user_login',
			'user_pass',
			'user_nicename',
			'user_email',
			'user_url',
			'user_registered',
			'display_name'
		);
	}
	
	/*
	 * Given an identifier, checks whether it is an identifier for an attribute
	 * stored in the users table
	 *
	 * @param string $attribute field id
	 *
	 * @return bool
	 */
	private function in_users_table($attribute) {
		if (in_array($attribute, $this->users_table_attributes()))
			return true;
		return false;
	}

	/*
	 * Load the field value from the data source.
	 *
	 * @param string $field_id the identifier for the field to load data for
	 * @param string $source_id the id of the object for which the metadata is to be loaded
	 * @param string $context the context in which the data will be loaded
	 *
	 * @return string the field value (or false if unable to load) 
	 */
	public function load_data($value, $field_id, $source_id, $context, $options = array()) {
	 	if (!$source_id)
	 		$source_id = $this->get_data_source_id();
		if ($field_id == 'user_name')
			return get_userdata($source_id)->first_name . ' ' . get_userdata($source_id)->last_name; 
		if ($this->in_users_table($field_id))
			return get_userdata($source_id)->$field_id;
 		return get_user_meta($source_id, $field_id, true);
	}
	
	/*
	 * Save the field value to the data source.
	 *
	 * @param string $field_id the identifier for the field to save data for
	 * @param string $destination_id the id of the object for which the metadata is to be saved
	 * @param string $value the value to save
	 *
	 * @return bool true if data saved
	 */
	public function save_data($field_id, $destination_id, $value) {
		if( is_numeric($destination_id) && !empty($value) ) {
			if ($field_id == 'user_name') {
				$name = explode(' ', $value);
				wp_update_user( array('ID' => $destination_id, 'first_name' => array_shift($name), 'last_name' => implode(' ',$name), 'display_name' => $value ) );
			} elseif ($this->in_users_table($field_id)) {
				wp_update_user( array('ID' => $destination_id, $field_id => $value ) );
			} else { 
				update_user_meta($destination_id, $field_id, $value); 
			}  	
		}
	}
}

/*
 * Use current user's data for a field. Extends WDTC_User_Data
 *
 * @since DTC Posts and Fields 0.5
 */
class WDTC_Current_User_Data extends WDTC_User_Data {

	/*
	 * Get the id for the instance of whatever object the data for the field is being 
	 * drawn from - in this case the current user
	 *
	 * @return int user ID
	 */
	public function get_data_source_id() {
		return get_current_user_id();
	}
}


/*
 * Use post author's user data for a field. Extends WDTC_User_Data
 *
 * @since DTC Posts and Fields 0.5.3
 */
class WDTC_Post_Author_Data extends WDTC_User_Data {
	
	/*
	 * Get the id for the instance of whatever object the data for the field is being 
	 * drawn from - in this case the user for the post object's author
	 *
	 * @return int user ID
	 */
	public function get_data_source_id() {
		global $post;
		return $post->post_author;
	}
}