 <?php

class WDTC_Walker extends Walker {
 
    /**
     * What the class handles.
     *
     * @since 2.1.0
     * @access private
     * @var string
     *
     * @see Walker::$tree_type
     */
    public $tree_type = 'custom';
 
    /**
     * Database fields to use.
     *
     * @since 2.1.0
     * @access public
     * @todo Decouple this
     * @var array
     *
     * @see Walker::$db_fields
     */
    public $db_fields = array ('parent' => 'parent_id', 'id' => 'id');
}

class WDTC_Walker_Basic_Dropdown extends WDTC_Walker {
    /**
     * Starts the element output.
     *
     * @since 2.1.0
     * @access public
     *
     * @see Walker::start_el()
     *
     * @param string $output   Passed by reference. Used to append additional content.
     * @param object $category Category data object.
     * @param int    $depth    Depth of category. Used for padding.
     * @param array  $args     Uses 'selected', 'show_count', and 'value_field' keys, if they exist.
     *                         See wp_dropdown_categories().
     * @param int    $id       Optional. ID of the current category. Default 0 (unused).
     */
 public function start_el( &$output, $object, $depth = 0, $args = array(), $id = 0 ) {
        $pad = str_repeat('&nbsp;', $depth * 3);
        $output .= "\t<option class=\"level-$depth\" value=\"" . esc_attr( $object->id ) . "\"";
        // Type-juggling causes false matches, so we force everything to a string.
        if ( (string) $object->id === (string) $args['selected'] )
            $output .= ' selected="selected"';
        $output .= '>';
        $output .= $pad.$object->name;
        $output .= "</option>\n";
    }
}

class WDTC_Walker_Basic_Radio_Buttons extends WDTC_Walker {
    /**
     * Starts the element output.
     *
     * @since 2.1.0
     * @access public
     *
     * @see Walker::start_el()
     *
     * @param string $output   Passed by reference. Used to append additional content.
     * @param object $category Category data object.
     * @param int    $depth    Depth of category. Used for padding.
     * @param array  $args     Uses 'selected', 'show_count', and 'value_field' keys, if they exist.
     *                         See wp_dropdown_categories().
     * @param int    $id       Optional. ID of the current category. Default 0 (unused).
     */
    public function start_el( &$output, $object, $depth = 0, $args = array(), $id = 0 ) {
        $pad = str_repeat('&nbsp;', $depth * 3);
        $output .= "\t<input type=\"radio\" name=\"" . $args['field_id'] . "\" class=\"level-$depth\" id=\"" . esc_attr( $object->id ) . "\"value=\"" . esc_attr( $object->id ) . "\"";
        // Type-juggling causes false matches, so we force everything to a string.
        if ( (string) $object->id === (string) $args['selected'] )
            $output .= ' checked';
        $output .= '>';
        $output .= $pad.$object->name;
        $output .= "\n";
        $output .= '<br>';    }
}