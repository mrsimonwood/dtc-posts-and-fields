<?php

/*
 * Class for managing patterns involving special characters (eg. for validating usernames)
 *
 * @since Doctoral_Training_Post_Types 0.4.4
 */
 
class WDTC_Pattern extends WDTC_Standard {
	
	/*
	 * The special characters permitted by the pattern
	 *
	 * @var string
	 */
	private $special_characters;
	
	/*
	 * Array optionally specifying url and/or prefix
	 */
	private $extras;
	
	/*
	 * Constructor
	 *
	 * @param string id the pattern id
	 * @param array extras
	 */
	public function __construct($id,array $extras=array()) {
		parent::__construct($id,'');
		$this->special_characters = array();
		if (strpos($id, 'alpha_num_standard') !== false)
			$this->special_characters = array(' ','\'', ',', ':','-');
		if (strpos($id, 'alpha_num_at') !== false)
			$this->special_characters = array('@');
		if (strpos($id, 'alpha_num_dot') !== false)
			$this->special_characters = array('.');
		if (strpos($id, 'alpha_num_underscore') !== false)
			$this->special_characters = array('_');
		if (strpos($id, 'alpha_num_hyphen') !== false)
			$this->special_characters = array('_','-');
		$this->extras = $extras;
	}
	
	/*
	 *  Getter for the special characters allowed in the pattern
	 *
	 * @return string
	 */
	public function get_special_characters() {
		return $this->special_characters;
	}
	
	/*
	 * Generate a string based on the special characters
	 *
	 * @param string $sep the separator to insert between the special characters
	 *
	 * @return string based on the special characters
	 */
	private function pattern_string($sep='') {
		if ($sep) {
			$sep = $this->enquote($sep);
			return addslashes($this->enquote(implode($sep,$this->get_special_characters())));
		}
		return implode($this->get_special_characters());
	}
	
	/*
	 * Take a string a put it in double quotes.
	 *
	 * @param string $string string to enquote
	 *
	 * @return string enquoted string
	 */
	private function enquote($string) {
		return '"' . $string . '"';
	}
	
	
	/*
	 * Return an explantory message indicating which non-alphanumeric characters are not prohibited.
	 *
	 * @param string $sep separator between each permitted character
	 * @param string $field_id slug for the field to which the pattern is being applied
	 * @param string $field_type the type of field to whih the pattern is being applied
	 *
	 * @return string message with the exceptions to prohibited non-alphanumeric characters
	 */
	public function get_exceptions_string($sep=' and ', $field_id='', $field_type='') {
		$exceptions = 'Only letters and numbers and ' . $this->pattern_string($sep) . ' allowed';
		if ($field_id) {
			$exceptions .= ' for \"' . $field_id . '\"';
			if ($field_type) 
				$exceptions .= ' ' . $field_type;
		}
		$exceptions .= '.';
		return $exceptions;
	}
	
	/*
	 * Javascript to validate against the pattern
	 *
	 * @return string js code
	 */
	public function pattern_jQuery() {
		$javascript = 'jQuery.validator.addMethod("' . $this->get_id() . '", function( value, element ) {';
		$javascript .= '	var regex = new RegExp(' . $this->get_regex() . ');';
		$javascript .= '	var key = value;';
		$javascript .= '	if (!regex.test(key)) {';
		$javascript .= '	   return false;';
		$javascript .= '	}';
		$javascript .= '	return true;';
		$javascript .= '}, "' . $this->get_exceptions_string() . '");';
		return $javascript;	
	}
	
	/*
	 * Check whether any given string matches the pattern
	 *
	 * @param string $string string to check
	 *
	 * @return bool whether the pattern matches
	 */
	public function match($string) {
		return preg_match($this->get_regex(),$string); 
	}
	
	/*
	 * The regular expresion for the pattern
	 *
	 * @return string regular expression
	 */
	private function get_regex() {
		$regex = '"^';
		$regex .= $this->get_url_regex($this->extras['url']);
		$regex .= $this->get_prefix_regex($this->extras['prefix']);
		$regex .= '[a-zA-Z0-9'. $this->pattern_string() . ']';
		$regex .= '*$"';
 		return $regex;
	}
	
	/*
	 * Regular expression for the given URL
	 *
	 * @param string $url base url
	 *
	 * @return string regular expression
	 */
	private function get_url_regex($url) {
		if (!filter_var($url, FILTER_VALIDATE_URL))
			return '';
		// get the domain and the path
		$url_pieces = parse_url($url);
		$domain = isset($url_pieces['host']) ? $url_pieces['host'] : '';
		$path = isset($url_pieces['path']) ? $url_pieces['path'] : '';
		// get the domain (from the subdomain + domain)
		if (preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $domain, $regs)) {
			$domain = $regs['domain'];
			// construct url string
			return '((https?:\/\/)?(www\.)?' . str_replace('.','\.',$domain) . $path . '/)?';
		}
		return '';	
	}

	/*
	 * Regular expression for any prefix for the field content
	 *
	 * @param string $prefix the prefix to match
	 *
	 * @return string regular expression
	 */
	private function get_prefix_regex($prefix) {
     	if ($prefix)
     		return '(' . $this->extras['prefix'] . ')?';
     	return '';
	}

}