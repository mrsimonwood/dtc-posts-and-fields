<?php

class WDTC_Fixes {

	/*
	 * The edit screen won't load completely if there's an error with the thumbnail.
	 * Check for errors and clear thumbnail field if there's a problem.
	 *
	 * @param WP_Post $post the post to edit
	 */
	public static function thumbnail_fix($post)
	{
		$thumbnail_id = get_post_meta( $post->ID, '_thumbnail_id', true );
		if ( is_wp_error( $thumbnail_id ) ) {
			update_post_meta( $post->ID, '_thumbnail_id', '' );
		}
	}	

	/*
	 * wp_title seems to fail if a query specifies both a post type and a taxonomy term.
	 * This function fixes this...
	 *
	 * @param string $title the page title
	 */
	public static function titles_fix( $title ) 
	{
		// If this is the reason for the fail, we'll use the post type. If that's not there,
		// we'll try the term title.
		if ($title=='')
		{
			if (get_query_var('post_type'))
			{
				$title = get_post_type_object(get_query_var('post_type'))->labels->name;
			} else {
				$title = single_term_title('',false);
			}
		}
		// We also want profiles to display as 'Student ' profiles...
		if ( get_query_var('post_type') == 'profile' ) 
		{
				$title = 'Student ' . $title;
			if ( get_query_var('pathway'))
			{
				$title = single_term_title('',false) . ' ' . $title;
			}
			}
		return $title;
	}
}
add_action('edit_form_after_title','WDTC_Fixes::thumbnail_fix');
add_filter('wp_title', 'WDTC_Fixes::titles_fix');

class WDTC_String_Formatter {
	/*
	 * For names in the format Surname, Forename return Forename Surname
	 *
	 * @param string $name the input name
	 *
	 * @return string the reformated name
	 */
	public static function formatname ($name, $forenamefirst = true)
	{
		if ($forenamefirst) {
			$names = explode(', ',$name,2); 
			return $names[1] . ' ' . $names[0];
		} else {
			$names = explode(' ',$name);
			$surname = array_pop($names);
			return  $surname . ', ' . implode(' ', $names);
		}
	}

	/*
	 * Given a URL, return the HTML for a hyperlink to that URL (or optionally some link text).
	 *
	 * @param string $url the url to make into a hyperlink to
	 * @param string $linktext the text to make into a link
	 */
	public static function linkify ($url, $linktext = null)
	{
		if (!$linktext)
			$linktext = esc_url($url);
		if ($url)
		{
			return '<a href="' . esc_url($url) . '" rel="tag">' . $linktext . '</a>';
		}
		if (empty($url)) {
			return '(no URL available)';
		}
		return $url;
	}
}

class WDTC_Term_Lists {
	
	/* Returns an HTML string of top level taxonomy terms associated with a post and given 
	 * taxonomy. Terms are linked to their respective term listing pages. 
	 * Optionally prepend the original descendant, optionally linked to listing page.
	 *
	 * @param integer $id post id
	 * @param string $taxonomy taxonomy name
	 * @param string $before text to display before the actual tags are displayed.
	 * @param string $sep text or character to display between each tag link.
	 * @param string $after text to display after the last tag.
	 * @param array $descendant_args options
	 *
	 * @return string html
	 */
	public static function get_the_term_ancestors_list ( $id, $taxonomy, $before = '', $sep = ', ', $after = '', $descendant_args = array(), $ancestorlink = true)
	{
		$default_descendant_args = array ('show' => true, 'link' => false, 'sep' => ', ');
		$descendant_args = wp_parse_args( $descendant_args, $default_descendant_args );
		$terms = get_the_terms( $id, $taxonomy );
		if ( is_wp_error( $terms ) )
			return $terms;
		if ( empty( $terms ) )
			return false;
		$ancestors = array();
		foreach ( $terms as $term ) {
			if (!in_array($term->term_id, $ancestors))
			{
				$parent_id = $term->parent;
				if ($parent_id)
				{
					if ($descendant_args['show'])
					{
						if ($descendant_args['link'])
						{
							$descendant = '<a href="' . get_term_link($term) . '">' . $term->name . '</a>';
						} else {
							$descendant = $term->name;
						}
						$descendant = $descendant . $descendant_args['sep'];
					}
					while ($parent_id)
					{
						$term=get_term_by('id',$parent_id,$taxonomy);
						$parent_id = $term->parent;
					}
				}
				$ancestor = $term->name;
				if ($ancestorlink)
				{
					$link = get_term_link( $term, $taxonomy );
					if ( is_wp_error( $link ) )
						return $link;
					$ancestor = '<a href="' . esc_url( $link ) . '" rel="tag">' . $ancestor . '</a>';
				} 				
				$term_links[] = $descendant . $ancestor;
				$ancestors[] = $term->id;
			}
		}
		$term_links = array_unique($term_links);
		return $before . join( $sep, $term_links ) . $after;
	}

	/*
	 * Given a term in a taxonomy return the top level parent for that term
	 * 
	 * @param WP_Term $term
	 * @param string $taxonomy taxonomy name
	 *
	 * @return WP_Term the top level parent term
	 */
	public static function get_the_term_ancestor ($term, $taxonomy)
	{
		$parent_id = $term->parent;
		if ($parent_id)
		{
			while ($parent_id)
			{
				$term=get_term_by('id',$parent_id,$taxonomy);
				$parent_id = $term->parent;
			}
		}
		return $term;
	}

	/*
	 * Given an array of terms in a taxonomy return and array of the top level parent for 
	 * those terms
	 * 
	 * @param array $terms WP_Term objects
	 * @param string $taxonomy taxonomy name
	 *
	 * @return array the top level parent terms
	 */
	public static function get_the_terms_ancestors ($terms, $taxonomy) 
	{
		$parent_terms = array();
		foreach ($terms as $term)
		{
			$ancestor = self::get_the_term_ancestor ($term, $taxonomy);
			$parent_terms[$ancestor->term_id] = $ancestor;
		}
		return $parent_terms;
	}
}
 


/*
 * Make the id for the post the slug
 * Uses the wdtc_post_element_id hook implemented in the esrcwalesdtc theme.
 *
 * @param string $post_element_id the id for the post
 *
 * @return string the (modified) id for the post
 */
function profile_element_id( $post_element_id ) {
	global $post;
	return $post->post_name;
}
add_filter ( 'wdtc_post_element_id', 'profile_element_id');
