<?php

/*
 * Base class for adding custom post types or custom taxonomies
 *
 * @since Doctoral_Training_Post_Types 0.4
 *
 */

abstract class WDTC_Registerer extends WDTC_Standard {

	/*
	 * The arguments for registering the thing
	 *
	 * @var array
	 */
	private $args; 
	
	/*
	 * Customisations defining how posts & archives will display in different contexts
	 *
	 * @var array
	 */
	private $customisations;
		
	/*
	 * Constructor
	 *
	 * @param string $id id for the thing
	 * @param string $name (plural) name for the thing
	 * @param string $singular_name singular name for the thing
	 */
	public function __construct($id, $name, $singular_name, $customisations = NULL) {
		parent::__construct($id, $singular_name);
		$labels = $this->get_labels($name);
		$this->args = array(
			'labels' => $labels, 
			'rewrite' => array('with_front' => false ), 
			'public' => true
		);
		$this->customisations = $customisations;
	}

	/*
	 * Get the arguments to register the thing
	 *
	 * @return array the arguments
	 */
    protected function get_args() {
		return $this->args;
    }

	/*
	 * Setter for arguments to register the thing
	 *
	 * @param string $key which argument to set
	 * @param mixed $arg the argument value
	 */
	protected function set_arg($key,$arg) {
		$this->args[$key] = $arg;
	}

	/*
	 * Getter for the customisation info
	 *
	 * @return array the customisation info
	 */
	private function get_customisations() {
		return $this->customisations;
	}
	
	/*
	 * Setup hooks for the class, and archive customisations
	 */
	public function init() {
        add_action('init', array(&$this,'register_custom_thing'));
		$this->customisations($this->get_customisations());
	}
    
    /*
     * Returns labels for registering the custom thing
     *
     * @return array labels for registering the thing
     */
    private function get_labels($plural_name) {
    	$labels = array(
    		'name' => $plural_name,
    		'singular_name' => __( $this->get_name()),
    		'add_new_item' => __( 'Add New ' . $this->get_name()),
    		'edit_item' => __( 'Edit ' . $this->get_name()),
    		'new_item' => __( 'New ' . $this->get_name()),	
    		'view_item' => __( 'View ' . $this->get_name()),
			'search_items' => __( 'Search ' . $this->get_name()),
			'not_found'=> __( 'No ' . $this->get_id() . ' found'),
			'not_found_in_trash' => __( 'No ' . $this->get_id() . ' found in Trash')
    	);
    	return $labels;
    }
    
    /*
     * Implement to register the custom thing
     */
    abstract protected function register_custom_thing();
    
	/*
	 * Set up all the customisations defining how the thing is displayed in
	 * different contexts
	 */
	private function customisations($customisations) {
		if (is_array($customisations)) {
			foreach ($customisations as $customisation) {
				$reflect  = new ReflectionClass(array_shift($customisation));
				array_unshift($customisation, $this->get_id());
				$customiser = $reflect->newInstanceArgs($customisation);
				$customiser->init();
			}
		}
	}    
}

/*
 * For registering custom post types
 *
 * @since Doctoral_Training_Post_Types 0.4
 *
 */
class WDTC_Post_Type_Registerer extends WDTC_Registerer {

	/*
	 * The fields for the post type
	 *
	 * @var array
	 */
	private $fields;

	/*
	 * Constructor
	 *
	 * @param string $id id for the thing
	 * @param string $name (plural) name for the thing
	 * @param string $singular_name singular name for the thing
	 * @param array $supports post type features supported
	 * @param array $field_data the data for defining all the fields for the post type
	 * @param array $customisations how posts of this type will display in different contexts
	 */	 
	 
	public function __construct($id, $name, $singular_name, array $supports,array $field_data = array(), $customisations = NULL) {
		parent::__construct($id, $name, $singular_name, $customisations);
		$this->set_arg('supports', $supports);
		$this->set_arg('has_archive', true);
		$this->fields = $this->initialise_fields($field_data);
	}

	/*
	 * Getter for the field data
	 *
	 * @return array the field data
	 */
	private function get_fields() {
		return $this->fields;
	}
	
	private function get_context_checker() {
		return $this->context_checker;
	}
	
	/*
	 * Setup hooks for the class, metaboxes and fields
	 */
	public function init() {
		add_filter('the_content', array(&$this, 'add_fields_to_content'));
		$this->setup_metaboxes();
		$fields = $this->get_fields();
		$this->add_fields_to_metaboxes($fields);
		parent::init();
		do_action('wdtc_' . $this->get_id() . '_fields', $this->fields);
	}

    /*
     * Register the custom post type
     */
    function register_custom_thing() {
    	register_post_type( $this->get_id(),$this->get_args());
    }
    
    /*
     * Adds fields to the content
     *
     * @param string $the_contents the post content
     *
     * @return string the post contents with fields prepended and/or appended
     */
    public function add_fields_to_content($the_content) {
    	$before = $after = '';
    	if ($this->get_id() == get_post_type()) {
			foreach ($this->get_fields() as $field) {
				if (!$field->is_hidden('display')) {
					if (!$field->is_footer())
						$before = $this->display_field_html($before, $field);
					else
						$after = $this->display_field_html($after, $field);
				}
			}
		}
		return $before . $the_content . $after;
    }
    
    private function display_field_html($html, $field) {	
    	return $html . apply_filters('wdtc_' . $this->get_id() . '_' . $field->get_id() . '_field_html', $field->display_field_html());
    }
    
    /*
     * Setup metaboxes for the post type
     */
    public function setup_metaboxes() {
		if (is_admin()) {
			require_once dirname(dirname(__FILE__)) . '/admin/metaboxes.php';
			$metabox_args = array(
				'id' => 'wdtc_' . $this->get_id() . '_information',
				'title' => $this->get_name() . ' Information',
				'post_type' => $this->get_id()
			);
			$metabox = new WDTC_Metabox($metabox_args);
			add_action('load-post.php', array($metabox,'metabox_setup'));
			add_action('load-post-new.php', array($metabox,'metabox_setup'));
		}	
	}
	
	/*
	 * Create all the fields for the post type
	 */
	private function initialise_fields($field_data) {
		$initialiser = WDTC_Data_Field_Initialiser::Instance();
		foreach ($field_data as $field_args)
			$fields[] = call_user_func_array(array($initialiser,'setup_field'), $field_args);
		return $fields;
	}
	
	/*
	 * Hook each field into metabox (unless hidden in admin)
	 *
	 * @param array $fields the initialised fields
	 */
	private function add_fields_to_metaboxes(array $fields) {
		foreach ($fields as $field) {
			if (!$field->is_hidden('admin')) {
				add_action('wdtc_metabox_' . $this->get_id(), array($field, 'the_form_field'));
				add_action('wdtc_metabox_save_meta_' . $this->get_id(), array($field, 'save_field_value'), 10, 1);
			}
		}
	}
}

/*
 * For registering custom taxonomy
 *
 * @since Doctoral_Training_Post_Types 0.4
 *
 */
class WDTC_Taxonomy_Registerer extends WDTC_Registerer {
	/*
	 * Array of post types the taxonomy will be applied to
	 *
	 * @var array post types
	 */
	private $post_types;
	
	/*
	 * Constructor
	 *
	 * @param array $init the necessary data to register the taxonomy
	 * @param array $post_types post types the taxonomy will be applied to
	 */
 	public function __construct($id, $name, $singular_name, $hierarchical, array $post_types, $customisations = NULL) {
		parent::__construct($id, $name, $singular_name, $customisations);
		$this->set_arg('hierarchical', $hierarchical);
        $this->post_types = $post_types;
	}
	
	/*
	 * Getter for the post types
	 *
	 * @return the post types
	 */
	private function get_post_types() {
		return $this->post_types;
	}
	
    /*
     * Register the custom taxonomy
     */
    function register_custom_thing() {
        register_taxonomy( $this->get_id(),$this->get_post_types(),$this->get_args());
    }
}