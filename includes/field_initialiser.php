<?php

class WDTC_Data_Field_Initialiser {

	/*
	 * Array of data handlers for fields to use to load and save data from different
	 * sources
	 *
	 * @var array data handlers
	 */
	private $field_data_handlers;
	
	/* Constructor - private, this is a singleton class */
	private function __construct() {}
    
    /**
     * Call this method to get singleton
     *
     * @return WDTC_Data_Field_Initialiser
     */
    public static function Instance()
    {
        static $inst = null;
        if ($inst === null) {
            $inst = new WDTC_Data_Field_Initialiser();
        }
        return $inst;
    }

	/*
	 * Add a data handler for fields to load save data from a particular source
	 *
	 * @param WDTC_Field_Data_Hander the data handler object
	 */
	public function add_data_handler($field_data_type, $field_data_handler) {
		$this->field_data_handlers[$field_data_type] = $field_data_handler;
	}
	
	/*
	 * Get a data hander
	 *
	 * @param string $type the identifier for the data handler to retreive.
	 *
	 * @return WDTC_Field_Data_Handler the data handler
	 */
	private function get_field_data_handler($type) {
		return $this->field_data_handlers[$type];		
	}

	/*
	 * Create a new field and hook in the data hander for loading and saving
	 *
	 * @param string $type the Class for the field to be created
	 * @param string $slug the identifier for the field
	 * @param string $name the name used to refer to the field on the front end
	 * @param string $field_data_type the idenfiier for the type of data handler to use for load and saving
	 * @param array $options options
	 *
	 * @return WDTC_Attribute the field object created
	 */
	public function setup_field ($type, $slug, $name, $field_data_type, array $options = array()) {
		$field = new $type($slug, $name, $options);
		$field_data = $this->get_field_data_handler($field_data_type);
		add_filter('load_value_for_' . 	$field->get_id(), array($field_data, 'load_data'), 10, 5);
		add_filter('save_value_for_' . 	$field->get_id(), array($field_data, 'save_data'), 10, 3);
		if ($type == 'WDTC_Custom_Dropdown') {
			add_filter('choices_for_' . $field->get_id(), array($field_data, 'get_choices'), 10, 2);
			add_filter('walker_for_' . $field->get_id(), array($field_data, 'get_walker'));
		}
		return $field;
	}
}