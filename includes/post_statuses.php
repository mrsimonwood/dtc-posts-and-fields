<?php
/*
 * Class for registering custom posts statuses
 *
 * @since Doctoral_Training_Post_Types 0.5
 */
class WDTC_Post_Status_Registerer extends WDTC_Standard {
	
	/*
	 * Name used when counting posts with this status (eg. 'published' 'archived' etc)
	 *
	 * @var string
	 */
	private $count_name;
	
	/*
	 * Post types for which status is to be available
	 *
	 * @var array
	 */
	private $post_types;

	/*
	 * Constructor
	 *
	 * @param string $id the status id
	 * @param string $name the name for the status
	 * @param string $count_name name used when counting posts with this status
	 * @param array $post_types post types for which the status will be available
	 */
	public function __construct($id, $name, $count_name, array $post_types) {
		parent::__construct($id,$name);
		$this->count_name = $count_name;
		$this->post_types = $post_types;
	}
	
	/*
	 * Getter for count name
	 *
	 * @return string The name to use when counting posts of this status
	 */
	private function get_count_name() {
		return $this->count_name;
	}

	/*
	 * Getter for post types
	 *
	 * @return array Post types for which this status is available
	 */
	private function get_post_types() {
		return $this->post_types;
	}
	
	/*
	 * Hooks for the class
	 */
	public function init() {
		add_action( 'init', array(&$this, 'register_status' ));
		add_action( 'admin_footer-post.php', array(&$this, 'append_post_status_list'));
		add_filter( 'display_post_states', array(&$this, 'display_post_status_state' ));
		add_action( 'admin_footer-edit.php', array(&$this, 'append_post_status_bulk_edit' ));
	}
	
	/*
	 * Register the custom post status
	 */
	public function register_status() {
		register_post_status( $this->get_id(), array(
			'label'                     => _x( $this->get_name(), 'post' ),
			'public'                    => true,
			'exclude_from_search'       => false,
			'show_in_admin_all_list'    => true,
			'show_in_admin_status_list' => true,
			'label_count'               => _n_noop( $this->get_count_name() . ' <span class="count">(%s)</span>', $this->get_count_name() . ' <span class="count">(%s)</span>' ),
		) );
	}

	/*
	 * Javascript to add the new custom post status to the available statuses,
	 * if available for the type of the post being edited.
	 */
	public function append_post_status_list(){
		 global $post;
		 $complete = '';
		 $label = '';
		 if (in_array($post->post_type, $this->get_post_types())) {
			  if($post->post_status == $this->get_id()){
				   $complete = ' selected="selected"';
				   $label = '<span id="post-status-display"> ' . $this->get_count_name() . ' </span>';
			  }
			  echo '
			  <script>
			  jQuery(document).ready(function($){
				   $(\'select#post_status\').append(\'<option value="' . $this->get_id() . '" '.$complete.'="">' . $this->get_name() . '</option>\');
				   $(\'.misc-pub-section label\').append(\''.$label.'\');
			  });
			  </script>
			  ';
		 }
	}

	/*
	 * Display the name of the custom post status where posts have this status
	 *
	 * @param array $states the statuses to display of the post
	 *
	 * @return array the modified statuses to display for the post
	 */ 
	function display_post_status_state( $states ) {
		 global $post;
		 $arg = get_query_var( 'post_status' );
		 if($arg != $this->get_id()){
			  if($post->post_status == $this->get_id()){
			  		$states[] = $this->get_name();
				return $states;
			  }
		 }
		return $states;
	}

	/*
	 * Javascript to add the new custom post status to 
	 * Bulk and Quick Edit boxes: Status dropdown.
	 */
	function append_post_status_bulk_edit() {
		$screen = get_current_screen();
		if (in_array($screen->post_type, $this->get_post_types())) {		
			echo '
			<script>
			jQuery(document).ready(function($){
				$(".inline-edit-status select ").append("<option value=\"' . $this->get_id() . '\">' . $this->get_name() . '</option>");
			});
			</script>
			';
		}
	}
}
