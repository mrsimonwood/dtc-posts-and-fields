<?php

/*
 * Standard class for this and dependent plugins for objects with a name and id.
 *
 * @since Doctoral_Training_Post_Types 0.5
 */
 
 abstract class WDTC_Standard {
 
 	/*
	 * Status registerer ID
	 *
	 * @var string
	 */
	protected $id;
	
	/*
	 * Status registerer name
	 *
	 * @var string
	 */
	protected $name;
 
 	/*
	 * Constructor
	 *
	 * @param string $id id for the object
	 * @param string $name the name for the object
	 */
	public function __construct($id, $name) {
		$this->id = $id;
		$this->name = $name;
	}

	/*
	 * Getter for the id
	 *
	 * @return the id for the object
	 */
	public function get_id() {
		return $this->id;
	}
	
	/*
	 * Getter for the name
	 *
	 * @return the name of the object
	 */
	public function get_name() {
		return $this->name;
	}
}