 <?php
 
/*
 * Class for managing the javascript for the form the fields appear in...
 *
 * @since Doctoral_Training_Post_Types 0.4
 */
class WDTC_Form_Helper {

	/*
	 * Stores the single instance of the class
	 *
	 * @var WDTC_Form_Helper
	 */
    private static $_instance = null;
    
    /*
     * Stores array of validation info
     *
     * @var array
     */
    private $validation_info;  
	
	/*
	 *
	 * @var bool
	 */
	private $datepicker_enabled;
    
    /*
     * Get single instance of the class - singleton
     *
     * @return WDTC_Form_Helper
     */
    public static function getInstance() {
        if (self::$_instance === null) self::$_instance = new WDTC_Form_Helper();
        return self::$_instance;
    }
    
    /* Constructor is private - singleton class */
    private function __construct() { }
  
  	/*
  	 * Setter for the validation info
  	 *
  	 * @param $pattern the validation pattern
  	 * @param $rule the validation rule
  	 * @param $message the message to show if validation fails
  	 */
    public function set_field_validation_info($pattern,$rule,$message) {
		if (!$this->validation_info) {
			if (!is_admin())
	   			add_action('wp_footer', array(&$this, 'validation'));
	   		$this->validation_info = array('patterns'=>array(),'rules'=>array(),'messages'=>array());
	   	}
		if ($pattern)
			$this->validation_info['patterns'][] = $pattern;
		if ($rule)
			$this->validation_info['rules'][] = $rule;
		if ($message)
			$this->validation_info['messages'][] = $message;
    }
    
    /*
     * Wipe the validation info
     */
    private function reset_validation_info() {
    	$this->validation_info = null;
    }
    
    /*
     * Getter for validation info
     *
     * @param string $info the identifier for the info to retrieve
     *
     * @return string the validation info
     */
    private function get_validation_info($info) {
    	return $this->validation_info($info);
    }

	/*
	 * Echo the validation js and set the validation info to null
	 */
    public function validation() {
		wp_enqueue_script('jquery-validate');
		echo '<script type="text/javascript">';
		echo '	jQuery(document).ready(function($) {';
		$patterns = array_unique($this->get_validation_info('patterns'));
		foreach ($patterns as $pattern) {
			echo $pattern;
		}
		echo '	$(\'#wdtcpeform\').validate({';
		echo '      rules: {';
		echo implode(",", $this->get_validation_info('rules'));
		echo '		}, ';
		echo '		messages: {';
		echo implode(",", $this->get_validation_info('messages')); 
		echo '		}';
		echo '  });';
		echo '});';
		echo '</script>';
		$this->reset_validation_info;
    }

	public function enable_datepicker() {
		if (!$this->datepicker_enabled) {
			if (is_admin()) {
				add_action('admin_footer', array(&$this, 'datepicker'));
			} else {
				add_action('wp_footer', array(&$this, 'datepicker'));
			}
			$this->datepicker_enabled = true;
		}
	}
	
	/*
	 * Javascript for date fields.
	 */
	public function datepicker()
	{
		echo '<script type="text/javascript">';
		echo '	jQuery(document).ready(function() {';
		echo '	jQuery(\'.wdtcdate\').datepicker({';
		echo '		dateFormat : \'dd-mm-yy\'';
		echo '  });';
		echo '});';
		echo '</script>';
		$this->datepicker_enabled = false;
	}
}