<?php

/*
 * Parent class for handling the display of metadata or taxonomy terms...
 *
 * @since Doctoral_Training_Post_Types 0.4
 */

abstract class WDTC_Attribute extends WDTC_Standard {
	 
	/*
	 * Options
	 *
	 * @var array
	 */
	private $options;
	
	/*
	 * Constructor
	 *
	 * @param string $id identifier for the field
	 * @param string $name well formatted name to display publicly
	 */
	public function __construct($id,$name, array $options = array()) {
        parent::__construct($id,$name);
        $this->options = $options;
    }
	
	/*
	 * Getter for the options
	 *
	 * @return array the options
	 */
	protected function get_options() {
		return $this->options;
	}
	
	/*
	 * Return a particular option for the field
	 *
	 * @param string $id identifier for the option
	 *
	 * @return the option value
	 */
	protected function get_option($id) {
		if (isset($this->options[$id]))
			return $this->options[$id];
	}

	/*
	 * Get the options that are needed by the field data object.
	 *
	 * @return array subset of the options
	 */
	private function get_field_data_options() {
		$field_data_keys = array('tax_display'); // Keys for options that need to be passed to the field data object
		return $this->sub_array($this->get_options(), $field_data_keys);
	}
	
	/*
	 * For an array, return an array with that subset which has certain keys NB Probably doesn't belong in this class!
	 *
	 * @param array $input the array to draw the subset from
	 * @param array $keys the keys to try to match
	 *
	 * @return array the subset
	 */
	private function sub_array(array $input, array $keys)
	{
		return array_intersect_key($input, array_flip($keys));
	}

	/*
	 * Whether the field should display after the main content for the object to which
	 * it is attached, rather than before
	 *
	 */
	public function is_footer() {
		return $this->get_option('footer_field');
	}


	/*
	 * Determine whether the field should be hidden wtihin a particular context (or always)
	 *
	 * @param array $context the contexts in which to field should not be visible
	 *
	 * @return bool whether to hide the field
	 */
	public function is_hidden($context = null) {
		$hidden = $this->get_option('hidden');
		if (!$hidden) 
			return false;
		if (is_array($hidden))
			if (!in_array($context,$hidden))
				return false;
		return true;
	}
			
	/*
	 * Get the value for the field
	 *
	 * @param integer $source_id id for the object for which to return the field's value
	 *
	 * @return string|integer the value
	 */
	public function load_field_value($source_id = NULL,$context = NULL) {
		$options = $this->get_field_data_options();
		$value = apply_filters('load_value_for_' . $this->get_id(), '', $this->get_id(), $source_id, $context, $options);
		if (empty($value) && $context == 'form' && $this->get_option('default'))
			$value = $this->get_option('default');
		return $value;
	}
	
	/*
	 * Save the field's value.
	 *
	 * @param $destination_id the id of the object to save to (default id of the current post)
	 */
	public function save_field_value($destination_id) {
	 	if( $destination_id && isset ( $_REQUEST[$this->get_id()] ) ) {
	 		$value = $_REQUEST[$this->get_id()];
	 		return $this->do_save($destination_id, $value);
		}
		return false;
	 }
	 
	/*
	 * Save the field value as post meta.
	 *
	 * @param $destination_id the id of the object to save to
	 */
	protected function do_save($destination_id, $value) {
	  	$value = trim($value);
  		$value = stripslashes($value);
  		$value = htmlspecialchars($value);
		do_action('save_value_for_' . $this->get_id(), $this->get_id(), $destination_id, $value);
		return $value;
	}

	/*
	 * Get the html for displaying the field value(s)
	 *
	 * @return string html
	 */
	public function display_field_html($html = '', $post_type = NULL) {
		$value = $this->load_field_value(NULL, 'display');
		if ($this->get_option('numerical_up_to') == true && is_numeric($value))
			if ($value > 1)
				$value = 'up to ' . $value;
		if ($value)
			$html .= $this->field_tags(true) . $this->get_label() . $value . $this->field_tags(false);
		return $html;
	}
	
	/*
	 * Return the html for the field in admin/metabox.
	 */ 
	public function get_form_field($source_id = NULL, $value = '', array $options = array('strong'=>true, 'br'=>true)) {
		if ($this->get_option('hidden')) {
			if (!is_array($this->get_option('hidden'))) {
				return;
			} else {
				if (in_array('form', $this->get_option('hidden')))
					return;
			}
		}
		if (!$value)
			$value = $this->load_field_value($source_id);
		if ( is_wp_error($value) )
			return;
		$html = '<p>';
		if ($options['strong'])
			$html .= '<strong>';
		$html .= '<label for="' . $this->get_id() . '">';
		$html .= __( $this->get_name(), 'default' );
		$html .= '</label>';
		if ($options['strong'])
			$html .= '</strong>';
		if ($this->get_description()) {
			$html .= '<br/>' . $this->get_description();
		}
		if ($options['br'])
			$html .= '<br/>';
		$html .= $this->get_form_field_input_element($value);
		$html .= '</p>';
		require_once dirname(__FILE__) . '/form_helpers.php';
		$this->enable_form_helper();
		return $html;	
	}

	/*
	 * Implement to return the specific html for the input in the form field
	 * 
	 * @param string|date the value to populate the field
	 */
	abstract protected function get_form_field_input_element($value);

	/*
	 * Echo the html for the field in admin/metabox.
	 */ 
	public function the_form_field() {
		echo $this->get_form_field();
	}

	/*
	 * Uses helper object for the javascript
	 *
	 */
	protected function enable_form_helper() {
		$form_helper = WDTC_Form_Helper::getInstance();
		$pattern = $rule = $message = '';
		if ($this->get_validation_pattern())
			$pattern = $this->get_validation_pattern(false)->pattern_jQuery();
		if ($this->validation_rules()) {
			$rule = '"' . $this->get_id() . '": {' . $this->validation_rules() . '}';
			if ($this->validation_message())
				$message = '"' . $this->get_id() . '": "' . $this->validation_message() . '"';
		}
		$form_helper->set_field_validation_info($pattern,$rule,$message);	
		return $form_helper;
	}
	
	/*
	 * Get the class name wrapped with html tags
	 *
	 * @return string html tag
	 */
	protected function get_label($with_colon = true){
		if ($with_colon)
			$colon = ':';
		return '<div class="field-label field-label-' . $this->get_id() . '">' . $this->get_name() . $colon . '</div> ';
	}

	/*
	 * Override this if you want to show a description...
	 *
	 * @return string field description
	 */
	protected function get_description() {
		return $this->get_option('description');
	}
		
	/*
	 * Tags for the field html
	 *
	 * @param bool $opening choose tags for opening or closing
	 *
	 * @return string html tags
	 */
	 protected function field_tags($opening) {
	 	if ($opening)
	 		return '<div class="field field-' . $this->get_id() . '">';
	 	return '</div>';
	 }
	 
	 /*
	  * Override to validate field input
	  *
	  * @return null or string with error message if validation fails
	  */
	 public function validation_error() {
	 	return null;
	 }
	 
	 /*
	  * Override to create validation rule (for JQuery validate)
	  *
	  * @return string validation rule
	  */
	 public function validation_rules() {
	 	return '';
	 }

	/*
	 * Override to compose the error meessage to be used if validation fails
	 *
	 * @return string validation failure message
	 */
	 public function validation_message() {
	 	return '';
	 }

	/*
	 * Override to provide the valididation pattern for the field.
	 *
	 * @param bool $string whether to return a string with the pattern name rather than a pattern object
	 * 
	 * @return WDTC_Pattern object or string the pattern name
	 */
	
	public function get_validation_pattern($string=true) {
		return null;
	}	
}

/*
 * Class for fields containing plain text value
 *
 * @since Doctoral_Training_Post_Types 0.4
 */
class WDTC_Custom_Text_Field extends WDTC_Attribute {

	public function get_validation_pattern($string=true) {
		if (!$string && $this->get_option('validation_pattern'))
			return new WDTC_Pattern($this->get_option('validation_pattern'));
		return $this->get_option('validation_pattern');
	}
		
	/*
	 * Output the html for the form field in admin/metabox
	 * 
	 * @param string|date the value to populate the field
	 *
	 * return string the html for the field input
	 */
	protected function get_form_field_input_element($value) { 
    	$html = '<input type="text" name="' . $this->get_id() . '" id="' . $this->get_id() . '"';
		$html .= ' class="wdtctext"';
		$html .= ' value="' . $value . '" size="20" />';
		return $html;
	}
	
	 /*
	  * Check the input value for the field validates, and if not return an error message
	  *
	  * @return null or string with error message if validation fails
	  */
	public function validation_error() {
		if ($this->get_validation_pattern() && $_REQUEST[$this->get_id()]) {
			$validation_pattern = new WDTC_Pattern ($this->get_validation_pattern());
			if (!$validation_pattern->match($_REQUEST[$this->get_id()]))
					return $this->validation_message();
  		}
  		return null;
	}

	 /*
	  * Provide validation rule (for JQuery validate)
	  *
	  * @return string validation rule
	  */	
	public function validation_rules() {
     	if ($this->get_validation_pattern()) {
			if ($this->get_option('required'))
				$rules .= 'required: true,';
			$rules .= $this->get_validation_pattern() . ': true';
			return $rules;
		}
	}
	
	/*
	 * Provide the error meessage to be used if validation fails
	 *
	 * @return string validation failure message
	 */
	public function validation_message() {
     	if ($this->get_validation_pattern()) {
			$validation_pattern = new WDTC_Pattern ($this->get_validation_pattern());
    		return $validation_pattern->get_exceptions_string(' and ', $this->get_name());
		}
	}
}

/*
 * Class for fields containing a text area
 *
 * @since Doctoral_Training_Post_Types 0.4.12
 */
class WDTC_Custom_Textarea_Field extends WDTC_Custom_Text_Field {

	/*
	 * Output the html for the form field in admin/metabox
	 * 
	 * @param string|date the value to populate the field
	 *
	 * return string the html for the field input
	 */
	protected function get_form_field_input_element($value) { 
    	$html = '<textarea type="text" name="' . $this->get_id() . '" id="' . $this->get_id() . '"';
		$html .= ' class="wdtctextarea"';
		$html .= ' style="';
		if ($this->get_option('width'))
			$html .= 'width: ' . $this->get_option('width') . '; ';
		if ($this->get_option('height'))
			$html .= 'height: ' . $this->get_option('height') . '; ';
		$html .= '"';
		$html .= '>';
		$html .= $value . '</textarea>';
		return $html;
	}
}


/*
 * Class for fields containing dates
 *
 * @since Doctoral_Training_Post_Types 0.4
 */
class WDTC_Custom_Date_Field extends WDTC_Attribute {

	/*
	 * Change the value for the field after loading it, before using it.
	 *
	 * @param string $value the field value loaded
	 * @param string $context where the value will be used.
	 */
	public function load_field_value($source_id = NULL,$context = NULL) {
		$value = parent::load_field_value($source_id,$context);
		$value = $this->default_date($value,$context);
		$value = $this->time_to_string($value,$context);
		return $value;
	}

	/*
	 * Take the input value, modify it before saving
	 *
	 * @param string $value value to convert
	 *
	 * @return integer converted value
	 */
	protected function do_save($destination_id, $value) {
		$value = $this->string_to_time($value);
		$value = parent::do_save($destination_id, $value);
		return $value;
	}
	
	/*
	 * If there is no date value to load, generate a default date value.
	 *
	 * @param string $value the field value loaded
	 * @param string $context where the value will be used
	 *
	 * return string default date value
	 */
	private function default_date($value, $context) {
		if ((empty($value) || $value == 0) && $context == 'form') {
			return strtotime('01-10-' . date(Y));
		}
		return $value;
	}
		
	/*
	 * Convert the value from string to time format
	 *
	 * @param string $value value to convert
	 *
	 * @return integer converted value
	 */
	private function string_to_time($value) {
		if (preg_match('(^\d{2}-\d{2}-\d{4}$)', $value) )
			return strtotime($value);
		return NULL;
	}
	
	/*
	 * Convert the value from time format to string
	 *
	 * @param integer $value value to convert
	 *
	 * @return string converted value
	 */
	private function time_to_string($value, $context) {
		$format = "d-m-Y";
		if ($context == 'display')
			$format = $this->get_option('display_format');
		if ($value)
			$value = date ($format,$value);
		return $value;
	}

	/*
	 * Output the html for the form field in admin/metabox
	 * 
	 * @param string|date the value to populate the field
	 *
	 * @return string html for the input in the field
	 */
	protected function get_form_field_input_element($value) {    
    	$html = '<input type="text" name="' . $this->get_id() . '" id="' . $this->get_id() . '"';
		$html .= ' class="wdtcdate wdtctext"';
		$html .= ' value="' . $value . '" size="20" />';
		return $html;
	}

	protected function enable_form_helper() {
		$form_helper = parent::enable_form_helper();
		$form_helper->enable_datepicker();
		return $form_helper;
	}

	 /*
	  * Check the input value for the field validates, and if not return an error message
	  *
	  * @return null or string with error message if validation fails
	  */
	public function validation_error() {
		if ($_REQUEST[$this->get_id()] && preg_match('(^\d{2}-\d{2}-\d{4}$)', $this->get_id()) ) {
			return "Date must be in the format dd-mm-yyyy";
  		}
  		return null;
	}
}

/*
 * Class for fields containing URLs
 *
 * @since Doctoral_Training_Post_Types 0.4
 */
class WDTC_Custom_URL_Field extends WDTC_Custom_Text_Field {


	/*
	 * Get the html for displaying the field value(s)
	 *
	 * @return string html
	 */
	public function display_field_html($html = '', $post_type = NULL) {
		$value = $this->load_field_value(NULL, 'display');
		if ($value) {
			$display = $value;
			if ($this->get_option('label_as_link')) {
				return $html . $this->field_tags(true) . ' <a href="' . $value . '">' . $this->get_label(false) . '</a>' . $this->field_tags(false);	
			}
			return $html . $this->field_tags(true) . $this->get_label() . ' <a href="' . $value . '">' . $display . '</a>' . $this->field_tags(false);
		}
		return $html;
	}
	
	 /*
	  * Check the input value for the field validates, and if not return an error message
	  *
	  * @return null or string with error message if validation fails
	  */
	public function validation_error() {
		if ($_REQUEST[$this->get_id()] && !filter_var($_REQUEST[$this->get_id()], FILTER_VALIDATE_URL))
			return $this->validation_message();
		return null;
	}
	
		/*
	 * Show a description/prompt...
	 *
	 * @return string field description
	 */
	protected function get_description() {
		return 'Enter the URL for your ' . $this->get_name();
	}

	 /*
	  * Provide validation rule (for JQuery validate)
	  *
	  * @return string validation rule
	  */
	public function validation_rules() {
     	if ($this->required)
        	$rules .= 'required: true,';
     	$rules .='url: true';
     	return $rules;	
	}

	/*
	 * Provide the error meessage to be used if validation fails
	 *
	 * @return string validation failure message
	 */
	public function validation_message() {	
    	return 'Please enter a valid website address for \'' . $this->get_name() . '\'.';
	}
}


/*
 * Class for fields containing URLs that aren't displayed (instead the field label is a hyperlink)
 *
 * @since Doctoral_Training_Posts_and_fields 0.4.18
 */
class WDTC_Custom_Hyperlink_Field extends WDTC_Custom_URL_Field {

}

/*
 * Class for fields containing username for webservices
 *
 * @since Doctoral_Training_Post_Types 0.4.3
 */
class WDTC_Custom_Username_Field extends WDTC_Custom_Text_Field {

	/*
	 * Take the input value, modify it before saving
	 *
	 * @param string $value value to convert
	 *
	 * @return string converted value
	 */
	protected function do_save($destination_id, $value) {
		$value = $this->extract_username($value);
		$value = parent::do_save($destination_id, $value);
		return $value;
	}
	
	/*
	 * Extract the username from the string given
	 *
	 * Will remove prefix if given, or extract from URL.
	 */
	public function extract_username($value) {
		// have we been given a URL?
		// if (filter_var($value, FILTER_VALIDATE_URL, FILTER_FLAG_HOST_REQUIRED)) {
		/*	$parsed_url = parse_url($value);
		if ($parsed_url) {
		echo $value;
		print_r($parsed_url); */
		$pattern = $this->get_validation_pattern(false);
		if ($pattern->match($value)) {
			if ($this->get_domain($value) == $this->get_domain($this->get_option('base_url'))) {
				$path = explode("/", $parsed_url['path']);
				$value = end($path);
			}
			$value = str_replace($this->get_option('usernameprefix'), "", $value);	
		}
		return $value;
	}

	/*
	 * Extract the domain from a url
	 *
	 * @param string $url the url from which to extract the domain
	 *
	 * return string the domain
	 */
	private function get_domain($url)
	{
		if (!preg_match('/^https?:\/\//', $url))
			$url = 'http://' . $url;
  		$url_pieces = parse_url($url);
		$domain = isset($url_pieces['host']) ? $url_pieces['host'] : '';
	    if (preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $domain, $regs)) {
    		return $regs['domain'];
     	}
	}
	
	/*
	 * Get the html for displaying the field value(s)
	 *
	 * @return string html
	 */
	public function display_field_html($html = '', $post_type = NULL) {
		$value = $this->load_field_value(NULL, 'display');
		if ($value) {
			$display = $this->get_option('usernameprefix') . $value;
			if ($this->get_option('base_url')) {
				$url = $this->get_option('base_url') . '/' . $value;
				$display = ' <a href="' . $url . '">' . $display . '</a>';
			}
			$html = $html .  $this->field_tags(true) . $this->get_label() . $display . $this->field_tags(false);
		}
		return $html;
	}

	/*
	 * Show a description/prompt...
	 *
	 * @return string field description
	 */
	protected function get_description() {
		return 'Enter your username for ' . $this->get_name();
	}

	 /*
	  * Check the input value for the field validates, and if not return an error message
	  *
	  * @return null or string with error message if validation fails
	  */
	public function validation_error() {
		$pattern = $this->get_validation_pattern(false);
		if ($_REQUEST[$this->get_id()] && !$pattern->match($this->extract_username($_REQUEST[$this->get_id()])))
			return $this->validation_message();
  		return null;
	}

	 /*
	  * Provide validation rule (for JQuery validate)
	  *
	  * @return string validation rule
	  */
	public function validation_rules() {
     	if ($this->required)
        	$rules .= 'required: true,';
     	$rules .= $this->get_validation_pattern() . ': true';
     	return $rules;	
	}

	/*
	 * Provide the error meessage to be used if validation fails
	 *
	 * @return string validation failure message
	 */
	public function validation_message() {
		$pattern = $this->get_validation_pattern(false);
    	return $pattern->get_exceptions_string(' and ',$this->get_name(),'username');
	}
	
	/*
	 * Provide the valididation pattern for the field.
	 *
	 * @param bool $string whether to return a string with the pattern name rather than a pattern object
	 * 
	 * @return WDTC_Pattern object or string the pattern name
	 */
	public function get_validation_pattern($string=true) {
		if (!$string && $this->get_option('validation_pattern'))
			return new WDTC_Pattern($this->get_option('validation_pattern') . '_' . str_replace('-','_',$this->get_id()),array('url'=>$this->get_option('base_url'),'prefix'=>$this->get_option('usernameprefix')));
		return $this->get_option('validation_pattern') . '_' . str_replace('-','_',$this->get_id());
	}
	
}

/*
 * Class for fields containing URLs
 *
 * @since Doctoral_Training_Post_Types 0.4.3
 */
class WDTC_Custom_Email_Field extends WDTC_Custom_Text_Field {

	/*
	 * Get the html for displaying the field value(s)
	 *
	 * @return string html
	 */
	public function display_field_html($html = '', $post_type = NULL) {
		$value = $this->load_field_value(NULL, 'display');
		return $html . $this->field_tags(true) . $this->get_label() . ' <a href="mailto:' . $value . '">' . $value . '</a>' . $this->field_tags(false);
	}

	 /*
	  * Check the input value for the field validates, and if not return an error message
	  *
	  * @return null or string with error message if validation fails
	  */
	public function validation_error() {
		$email = $_REQUEST[$this->get_id()];
		if ($email  && email_exists($email) && email_exists($email) != get_current_user_id())
			return 'This email address is in use by someone else';
		if ($_REQUEST[$this->get_id()] && !filter_var($email, FILTER_VALIDATE_EMAIL))
			return $this->validation_message();
		return null;
	}

	 /*
	  * Provide validation rule (for JQuery validate)
	  *
	  * @return string validation rule
	  */
	public function validation_rules() {
     	if ($this->required)
        	$rules .= 'required: true,';
     	$rules .='email: true';
     	return $rules;	
	}
	
	/*
	 * Provide the error meessage to be used if validation fails
	 *
	 * @return string validation failure message
	 */
	public function validation_message() {
    	$message = 'Please enter a valid email address for \'' . $this->get_name() . '\'.';
    	return $message;
	}
}

/*
 * Parent class for fields which offer a choice of options
 *
 * @since Doctoral_Training_Post_Types 0.4
 */
abstract class WDTC_Custom_Options_Field extends WDTC_Attribute {

	/*
	 * Change the value for the field after loading it, before using it.
	 *
	 * @param string $value the field value loaded
	 * @param string $context where the value will be used.
	 */
	 
	public function load_field_value($source_id = NULL,$context = NULL, $use_display_value = false) {
		$value = parent::load_field_value($source_id,$context);
		$choices = $this->get_option('choices');
		if (is_array($choices) && $use_display_value) {
			$value = $choices[$value];
		}
		return $value;
	}

	 
	/*
	 * Output the html for the form field in admin/metabox
	 * 
	 * @param string the value to populate the field
	 *
	 * @return string the input in the form field
	 */
	protected function get_form_field_input_element($value) {
		$html = $this->before_choices();
		$choices = apply_filters('choices_for_' . $this->get_id(), $this->get_option('choices'), $this->get_id());
		foreach($choices as $key => $choice) {
			if (is_string($choice)) {
				$choices[$key] = new WDTC_Custom_Element($key,$choice);
			} elseif (is_numeric($choice)) {
				$choices[$key] = new WDTC_Custom_Element($choice,$choice);
			}
		}
		$walker = apply_filters('walker_for_' . $this->get_id(), $this->get_walker());
		$args = array($choices, 0, array('selected'=>$value, 'field_id'=>$this->get_id(), 'value_field'=>'slug'));
		$html .= call_user_func_array( array( $walker, 'walk' ), $args );
		$html .= $this->after_choices();
		return $html;
	}
	
	/*
	 * Return the html for before the list of options
	 */
	protected function before_choices() {}

	abstract protected function get_walker();

	/*
	 * Return the html for after the list of options
	 */
	protected function after_choices() {}
}


/*
 * Class for fields which offer a dropdown list of options in the admin
 *
 * @since Doctoral_Training_Post_Types 0.4
 */
class WDTC_Custom_Dropdown extends WDTC_Custom_Options_Field {

	/*
	 * Return the html for before the list of options
	 */
	protected function before_choices() {
		return '<select name="' . $this->get_id() .'" id="' . $this->get_id() . '">';
	}

	protected function get_walker() {
		return new WDTC_Walker_Basic_Dropdown;
	}
	
	/*
	 * Return the html for after the list of options
	 */
	protected function after_choices() {
		return '</select>';
	}
}


/*
 * Class for fields which offer a choice of radio button options in the admin
 *
 * @since Doctoral_Training_Post_Types 0.4
 */
class WDTC_Custom_Radio_Buttons extends WDTC_Custom_Options_Field {

	/*
	 * Return the html for before the list of options
	 */
	protected function before_choices() {
		return '<span class="input-group">';
	}
	
	/*
	 * Return the html for each option that can be selected
	 *
	 * @param $key
	 * @param $name
	 * @param $value
	 */
	protected function get_walker() {
		return new WDTC_Walker_Basic_Radio_Buttons;
	}

	/*
	 * Return the html for after the list of options
	 */
	protected function after_choices() {
		return '</span>';
	}
}

/*
 * Custom element class for element objects used by the Walker
 *
 */
class WDTC_Custom_Element {

	/*
	 * Identifier
	 *
	 * @var string
	 */
	public $id;
	
	/*
	 * Parent id for hierarchical elements
	 *
	 * @var int
	 */
	private $parent_id;
	
	/*
	 * Display name
	 *
	 * @var string
	 */
	public $name;
	
	/*
	 * Constructor
	 *
	 * @param string $id identifier
	 * @param string $name display  name
	 */
	function __construct($id,$name) {
		$this->id = $id;
		$this->name = $name;
	}
}