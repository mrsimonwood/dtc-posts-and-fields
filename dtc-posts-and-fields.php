<?php
/*
 * DTP Posts and Fields
 *
 * @package   DTC_Posts_and_Fields
 * @author    Simon Wood <wordpress@simonwood.info>
 * @license   GPL-2.0+
 *
 * @wordpress-plugin
 * Plugin Name: DTP Posts and Fields
Bitbucket Plugin URI: https://bitbucket.org/mrsimonwood/dtc-posts-and-fields
 * Description: Provides tools for creating custom post types, fields and taxonomies. 
 * Version: 	0.5.9
 * Author: 		Simon Wood
 * Author URI: 	http://simonwood.info
 * License: 	GNU General Public License v2.0
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 */

/*
 * Load all the classes etc.
 *
 */
function load_posts_and_fields_classes() {
	require_once dirname(__FILE__) . '/includes/standard.php';
	require_once dirname(__FILE__) . '/includes/functions.php';
	require_once dirname(__FILE__) . '/includes/post_types.php';
	require_once dirname(__FILE__) . '/includes/post_statuses.php';
	require_once dirname(__FILE__) . '/includes/walker.php';
	require_once dirname(__FILE__) . '/includes/fields.php';
	require_once dirname(__FILE__) . '/includes/field_data.php';
	require_once dirname(__FILE__) . '/includes/field_initialiser.php';
	require_once dirname(__FILE__) . '/includes/patterns.php';
	require_once dirname(__FILE__) . '/includes/display.php';
	
		require_once dirname(__FILE__) . '/includes/display-alt.php';
	
	require_once dirname(__FILE__) . '/includes/shortcodes.php';
	if (is_admin())
		require_once dirname(__FILE__) . '/admin/titles.php';
}

/*
 * Call the function to load the plugin, do actions for dependent plugins
 *
 */
function dtc_posts_and_fields_init() {
	load_posts_and_fields_classes();
    do_action( 'dtc_posts_and_fields_init' );
}
add_action( 'plugins_loaded', 'dtc_posts_and_fields_init' );


/*
 * Register jquery validate script
 *
 */
function wdtcpaf_register_scripts() {
	wp_register_script('jquery-validate', plugin_dir_url( __FILE__ ) . 'js/jquery.validate.min.js', array('jquery'), '1.10.0', true);
}
add_action( 'wp_enqueue_scripts', 'wdtcpaf_register_scripts' );
