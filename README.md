# README #

### What is this repository for? ###

* Wordpress Plugin to provide tools for creating custom post types, fields and taxonomies. 
* Version 0.5.9

### How do I get set up? ###

* Upload to wp-content/plugins via FTP or Plugins > Add New > Upload Plugin
* In the dashboard choose "Activate" or "Activate Plugin"

