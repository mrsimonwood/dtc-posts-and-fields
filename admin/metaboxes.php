<?php

/*
 * Class for handling metaboxes with custom fields for custom post types...
 *
 * @since Doctoral_Training_Post_Types 0.4
 */

class WDTC_Metabox {
	/*
	 * Info about the metabox
	 *
	 * @var array
	 */
	private $settings;

	 /*
	  * Constructor
	  *
	  * @param array $settings data to set up metabox
	  */
    public function __construct(array $settings) {
        $this->settings = $settings;
    }
    
    /*
     * Getter for the settings
     *
     * @param string $setting name of the setting to get
     *
     * @return string the setting
     */
    private function get_setting($setting) {
    	return $this->settings[$setting];
    }
    
    /*
     * Hooks to add and save metabox data
     */
	public function metabox_setup() {
 		add_action( 'add_meta_boxes_' . $this->get_setting('post_type'), array(&$this,'add_meta' ) );
		add_action( 'save_post', array(&$this, 'save_meta'), 10, 2 );    
    }
    
	/*
	 * Add the metabox
	 *
	 */
    public function add_meta() {
    	if (is_string($this->get_setting('title'))) {
    		add_meta_box (
    			$this->get_setting('id'),
				esc_html__( $this->get_setting('title'), 'default' ),
				array(&$this, 'meta_box' ),
				$this->get_setting('post_type'),	
				'normal',
				'default'
			);
		}
    }
    
	/*
	 * Set up the metabox, fields
	 *
	 * @param WP_Post $post the post for which the metabox will manage data
	 * @param array $metabox metabox details
	 */
    public function meta_box( $post, $metabox ) {
		// The datepicker will be used for the custom date fields.
		wp_enqueue_script('jquery-ui-datepicker');
		wp_enqueue_style('jquery-style', 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css');
		$metabox_id = $metabox['id'];
		wp_nonce_field( basename( __FILE__ ), $metabox_id . '_nonce' );
		$values = get_post_custom( $post->ID );  
		do_action('wdtc_metabox_' . $this->get_setting('post_type'));
		// Javascript for the datepicker in the date fields
		echo '<script type="text/javascript">';
		echo '	jQuery(document).ready(function() {';
		echo '	jQuery(\'.wdtc-date\').datepicker({';
		echo '		dateFormat : \'dd-mm-yy\'';
		echo '  });';
		echo '});';
		echo '</script>';
    }
     
    /*
     * Save the metabox data
     *
     * @param integer $post_id the id of the post to which the data is being added
     * @param WP_Post $post the post to which the data is being added
     */
    public function save_meta( $post_id, $post ) {
		// Check if the current user has permission to edit the post.
		$post_type = get_post_type_object( $post->post_type );
		if ( !current_user_can( $post_type->cap->edit_post, $post_id ) )
			return $post_id;
		// Verify the nonce
		if ( isset( $_POST[$this->get_setting('id') . '_nonce'] ) && wp_verify_nonce( $_POST[$this->get_setting('id') . '_nonce'], basename( __FILE__ ) ) ) {
			do_action('wdtc_metabox_save_meta_' . $this->get_setting('post_type'), $post_id);
		}    
    }
}


