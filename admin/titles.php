<?php

/*
 * Class for generating titles from taxonomy terms when a post is saved...
 *
 * @since Doctoral_Training_Post_Types 0.4
 */

class WDTC_Title_From_Taxonomy_Terms {

	/*
	 * The post type for which to modify the title
	 *
	 * @var string
	 */
	private $post_type;

	/*
	 * The taxonomy terms to use in generating the title
	 *
	 * @var array
	 */
	private $taxonomy_terms;
	
	/*
	 * Constructor
	 *
	 * @param string $post_type the post type for which to modify the title
	 * @param array @taxonomy_terms the taxonomy terms to use in generating the title
	 */
	public function __construct($post_type,array $taxonomy_terms) {
		$this->post_type = $post_type;
		$this->taxonomy_terms = $taxonomy_terms;
	}
	
	/*
	 * Create the new title string and update the database
	 *
	 * @param integer $post_id the id of the post for which the title is being set
	 *
	 */
	function set_title($post_id) {
	    global $wpdb;
	    if ( get_post_type( $post_id ) == $this->post_type ) {
    		foreach ($this->taxonomy_terms as $taxonomy_term) {
				$terms = wp_get_object_terms($post_id, $taxonomy_term);
				$terms = WDTC_Term_Lists::get_the_terms_ancestors($terms, $taxonomy_term);
				foreach ($terms as $term) {
					$title_parts[] = $term->name;
				}
        	}
			$sep = ' - ';
			if ($title_parts)
				$title = join ($sep, $title_parts );
	        $where = array( 'ID' => $post_id );
    	    $wpdb->update( $wpdb->posts, array( 'post_title' => $title ), $where );
    	}
	}
}

